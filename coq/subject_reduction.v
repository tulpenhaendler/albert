Require Import Coq.Program.Equality.
Require Import albert aux label_lexico typer.
Require Import List.

Hint Constructors typing_val.

Lemma record_map_rewrite1 : forall l1 l2 v1 v2 ty1 ty2,
    map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__))
        ((l1, v1, ty1)::(l2, v2, ty2)::nil)
    = ((l1, v1) :: nil) ++ (l2, v2) :: nil.
Proof. intros; reflexivity. Qed.

Lemma record_map_rewrite2 : forall l1 l2 v1 v2 ty1 ty2,
    map (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__))
        ((l1, v1, ty1)::(l2, v2, ty2)::nil)
    = ((l1, ty1) :: nil) ++ (l2, ty2) :: nil.
Proof. intros; reflexivity. Qed.

Ltac map_length H :=
  erewrite <- map_length; symmetry; erewrite <- map_length at 1;
  rewrite H; symmetry; auto.

(** Subject reduction for lhs *)
Theorem sr_lhs: forall lhs G E v v' ty ty',
    typing_lhs G lhs ty ty' ->
    typing_val G v ty ->
    eval_lhs E lhs v v' ->
    typing_val G v' ty'.
Proof.
  intros lhs G E v v' ty ty' Hlhs Hval Heval.
  induction Heval.
  - (* var = val *)
    inversion Hlhs; subst.
    assert (map (fun pat_ : label * value * albert.ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__)) ((var, val, ty)::nil)
            = (var, val)::nil) as Hrew1 by reflexivity.
    assert (map (fun pat_ : label * value * albert.ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__)) ((var, val, ty)::nil)
            = (var, ty)::nil) as Hrew2 by reflexivity.
    rewrite <- Hrew1; rewrite <- Hrew2. constructor; simpl.
    intros val__ ty__ [Heq|false]; try (inversion false).
    inversion Heq; subst; assumption.
  - (* { l1 = x1 ; .. ; ln = xn } *)
    invert Hlhs. rename l__x__val__list into lv; rename l__x__ty__list into lt.
    invert Hval. rename l__val__ty__list into lvt.
    assert (length lv = length lt) as Hlen by map_length H1.
    assert (length lv = length lvt) as Hlen2 by map_length H.
    remember (combine lv lt) as lvlt.
    specialize (combine_inv_l _ _ lv lt Hlen) as Hcombl; specialize (combine_inv_r _ _ lv lt Hlen) as Hcombr.
    rewrite <- Hcombl. rewrite <- Hcombr at 2. repeat rewrite map_map.
    clear Hcombl; clear Hcombr; clear lvlt Heqlvlt.
    generalize Hlen Hlen2 H H1 H2 H3; clear Hlen Hlen2 H H1 H2 H3.
    generalize dependent lvt; generalize dependent lt; generalize dependent lv.
    refine (list_triple_ind _ _ _ _ _ _); intros; simpl in *.
    + apply typing_record_nil.
    + destruct a1 as [[a11 x1] a13]; destruct a2 as [[a21 x2] a23].
      assert (x1 = x2) as Ha by (invert H3; reflexivity); subst.
      apply typing_record_cons; auto.
      * apply H5. destruct a3 as [[a31 a32] a33]. invert H2; invert H3; invert H4.
        left. reflexivity.
      * apply H1; auto. inversion H2; auto. inversion H3; auto. inversion H4; auto.
Qed.

(** Subject reduction for argument *)
Theorem sr_arg : forall arg G E v v' ty ty',
    typing_arg G arg ty ty' ->
    typing_val G v ty ->
    eval_arg E arg v v' ->
    typing_val G v' ty'.
Proof.
  intros arg G E v v' ty ty' Harg Hval Heval.
  induction Heval.
  - (* var *)
    invert Harg. eapply val_record_nil_invert; eauto.
  - (* val *) invert Harg; auto.
  - (* record *)
   invert Harg. rename l__x__val__list into lv; rename l__x__ty__list into lt.
   invert Hval. rename l__val__ty__list into lvt.
    assert (length lv = length lt) as Hlen by map_length H1.
    assert (length lv = length lvt) as Hlen2 by map_length H.
    remember (combine lv lt) as lvlt.
    specialize (combine_inv_l _ _ lv lt Hlen) as Hcombl; specialize (combine_inv_r _ _ lv lt Hlen) as Hcombr.
    rewrite <- Hcombl. rewrite <- Hcombr at 2. repeat rewrite map_map.
    clear Hcombl; clear Hcombr; clear lvlt Heqlvlt.
    generalize Hlen Hlen2 H H1 H2 H3; clear Hlen Hlen2 H H1 H2 H3.
    generalize dependent lvt; generalize dependent lt; generalize dependent lv.
    refine (list_triple_ind _ _ _ _ _ _); intros; simpl in *.
    + apply typing_record_nil.
    + destruct a1 as [[l1 a12] a13]; destruct a2 as [[l2 a22] a23].
      assert (l1 = l2) as Ha by (invert H3; reflexivity); subst.
      apply typing_record_cons; auto.
      * apply H5. destruct a3 as [[a31 a32] a33]. invert H2; invert H3; invert H4.
        left. reflexivity.
      * apply H1; auto. inversion H2; auto. inversion H3; auto. inversion H4; auto.
Qed.

(** Subject reduction for function *)
Theorem sr_f : forall f G E v v' ty ty',
    typing_f G f ty ty' ->
    typing_val G v ty ->
    eval_f E f v v' ->
    typing_val G v' ty'.
Proof.
  intros arg G E v v' ty ty' Hf Hval Heval.
  induction Heval.
  - admit.
Admitted.

Lemma wf_record_cons_inv : forall G lt l ty,
    typing_ty_well_formed G (ty_rty (rty_record ((l, ty)::lt))) ->
    typing_ty_well_formed G (ty_rty (rty_record lt)).
Proof.
  intros G lt l ty Hwf.
  invert Hwf. invert H0.
  constructor; intuition.
  apply H1. simpl; auto.
Qed.

Corollary wf_record_cons2_inv : forall G lt l ty l' ty',
    typing_ty_well_formed G (ty_rty (rty_record ((l, ty)::(l', ty')::lt))) ->
    typing_ty_well_formed G (ty_rty (rty_record ((l, ty)::lt))).
Proof.
  intros G lt l ty l' ty' Hwf.
  invert Hwf.
  constructor; intuition.
  + constructor; invert H0; invert H5; auto.
    invert H4; auto.
  + apply H1. invert H. constructor; auto.
    simpl; auto.
Qed.

Lemma wf_not_In : forall G lt l ty ty',
    typing_ty_well_formed G (ty_rty (rty_record ((l, ty) :: lt))) ->
    ~ (In (l, ty') lt).
Proof.
  intros G lt l ty' ty'' Hwf.
  invert Hwf; clear H1 H3.
  eapply Sorted_not_In; eauto.
Qed.

Corollary wf_not_In_val : forall G lt l ty' val',
    typing_ty_well_formed G (ty_rty (rty_record ((l, ty'):: map (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__)) lt))) ->
    ~ (In (l, val') (map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__)) lt)).
Proof.
  intros G lt.
  induction lt; intros l ty' val' Hwf contra. simpl in *.
  - inversion contra.
  - simpl in Hwf; destruct a as [[l' v'] ty''].
    specialize (wf_record_cons2_inv G (map (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__)) lt) l ty' l' ty'' Hwf) as Hwf'.
    specialize (IHlt l ty' val' Hwf').
    apply wf_not_In with (ty' := ty'') in Hwf.
    apply Hwf; clear Hwf. destruct contra; simpl in *.
    + invert H; left; reflexivity.
    + exfalso. apply IHlt; auto.
Qed.

(** Subject reduction for rhs *)
Theorem sr_rhs : forall rhs G E v v' ty ty',
    typing_rhs G rhs ty ty' ->
    typing_val G v ty ->
    eval_rhs E rhs v v' ->
    typing_val G v' ty'.
Proof.
  intros rhs G E v v' ty ty' Hrhs Hval Heval.
  induction Heval.
  - (* arg *) inversion Hrhs; subst. eapply sr_arg; eauto.
  - (* f arg *)
    inversion Hrhs; subst. eapply sr_f; eauto.
    eapply sr_arg; eauto.
  - (* projection *)
    invert Hrhs; invert Hval; rename l__val__ty__list into lt.
    destruct rval5 as [lv5]; destruct rty5 as [lt5]; simpl in *.
    apply H4.
    assert ((val, ty') = (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (_, val__) := p in (val__, ty__)) (l, val, ty')) by reflexivity.
    assert (In (l, val, ty') lt).
    { apply Join_In_l_cons in H; apply Join_In_l_cons in H3.
      induction lt; simpl in *. inversion H. destruct a as [[l' val'] ty''].
      destruct H; destruct H3; try invert H; try invert H1; try invert H3; auto.
      + eapply wf_not_In in H6. apply H6 in H1; inversion H1.
      + eapply wf_not_In_val in H6. apply H6 in H; inversion H.
      + right. apply IHlt; auto. apply wf_record_cons_inv in H6; assumption. }
    eapply in_map with (f := (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (_, val__) := p in (val__, ty__))) in H1; assumption.
   - (* update *)
     destruct rval5 as [lv]; destruct rval' as [lv1]; destruct rval'' as [lv2].
     invert Hrhs; invert Hval.
     rename l__var__ty__list into lvt; rename l__var__val__val'__list into lvvv'; rename l__val__ty__list into lvt'.
     destruct rty5 as [lt]; destruct rty' as [lt'].
     rewrite app_nil_r in *.
     assert (length lvt = length lvvv') as Hlen by map_length H2.
     admit. (* like projection, but n times *)
  (* - (* add *) inversion Hrhs; subst; auto. *)
  (* - (* sub *) inversion Hrhs; subst; auto. *)
  (* - (* mul *) inversion Hrhs; subst; auto. *)
  (* - (* div *) *)
  (*   inversion Hrhs; subst; rewrite app_nil_r; rewrite app_nil_r in *; *)
  (*     erewrite <- record_map_rewrite1; erewrite <- record_map_rewrite2; *)
  (*       eapply typing_Tval_record; intuition; simpl in H; destruct H as [H|[H|H]]; *)
  (*         try (inversion H; subst; auto). *)
Admitted.

Lemma join_type_value : forall G lv lt lt1 lt2,
    typing_val G (val_rval (rval_record lv)) (ty_rty (rty_record lt)) ->
    Join lt1 lt2 lt ->
    (exists lv1 lv2, Join lv1 lv2 lv /\
        typing_val G (val_rval (rval_record lv1)) (ty_rty (rty_record lt1)) /\
        typing_val G (val_rval (rval_record lv2)) (ty_rty (rty_record lt2))).
Proof.
  intros G lv lt lt1 lt2 Hval HJoin.
  generalize dependent lv.
  induction HJoin; intros lv Hval.
  - exists lv. exists nil. split. constructor. split; auto. apply typing_record_nil.
  - exists nil. exists lv. split. constructor. split; auto. apply typing_record_nil.
  - specialize (typing_record_cons_ex_r G l1 t1 lv v3 Hval) as [v1' [lvs Heqv4]]; subst.
    apply typing_record_cons_inv in Hval; destruct Hval as [Hval Hval'].
    specialize (IHHJoin lvs Hval') as [lv1 [lv2 [H1 [H3 H4]]]].
    exists ((l1, v1')::lv1); exists lv2; intuition.
    apply typing_record_cons_ex_r in H4; destruct H4 as [v2' [lvs2 H4]]; subst.
    apply JoinLeft; auto.
    apply typing_record_cons; auto.
  - specialize (typing_record_cons_ex_r G l2 t2 lv v3 Hval) as [v2' [lvs Heqv4]]; subst.
    apply typing_record_cons_inv in Hval; destruct Hval as [Hval Hval'].
    specialize (IHHJoin lvs Hval') as [lv1 [lv2 [H1 [H3 H4]]]].
    exists lv1; exists ((l2, v2')::lv2); intuition.
    apply typing_record_cons_ex_r in H3; destruct H3 as [v1' [lvs1 H3]]; subst.
    apply JoinRight; auto.
    apply typing_record_cons; auto.
Qed.

Lemma join_value_type : forall G lv lv1 lv2 lt,
    typing_val G (val_rval (rval_record lv)) (ty_rty (rty_record lt)) ->
    Join lv1 lv2 lv ->
    (exists lt1 lt2, Join lt1 lt2 lt /\
        typing_val G (val_rval (rval_record lv1)) (ty_rty (rty_record lt1)) /\
        typing_val G (val_rval (rval_record lv2)) (ty_rty (rty_record lt2))).
Proof.
  intros G lv lv1 lv2 lt Hval HJoin.
  generalize dependent lt.
  induction HJoin; intros lt Hval.
  - exists lt; exists nil. split. constructor. split; auto. apply typing_record_nil.
  - exists nil; exists lt. split. constructor. split; auto. apply typing_record_nil.
  - specialize (typing_record_cons_ex_l G l1 t1 v3 lt Hval) as [v1' [lvs Heqv4]]; subst.
    apply typing_record_cons_inv in Hval; destruct Hval as [Hval Hval'].
    specialize (IHHJoin lvs Hval') as [lv1 [lv2 [H1 [H3 H4]]]].
    exists ((l1, v1')::lv1); exists lv2; intuition.
    apply typing_record_cons_ex_l in H4; destruct H4 as [v2' [lvs2 H4]]; subst.
    apply JoinLeft; auto.
    apply typing_record_cons; auto.
  - specialize (typing_record_cons_ex_l G l2 t2 v3 lt Hval) as [v2' [lvs Heqv4]]; subst.
    apply typing_record_cons_inv in Hval; destruct Hval as [Hval Hval'].
    specialize (IHHJoin lvs Hval') as [lv1 [lv2 [H1 [H3 H4]]]].
    exists lv1; exists ((l2, v2')::lv2); intuition.
    apply typing_record_cons_ex_l in H3; destruct H3 as [v1' [lvs1 H3]]; subst.
    apply JoinRight; auto.
    apply typing_record_cons; auto.
Qed.

Lemma join_concl: forall G lv lv1 lv2 lt lt1 lt2,
    typing_val G (val_rval (rval_record lv1)) (ty_rty (rty_record lt1)) ->
    typing_val G (val_rval (rval_record lv2)) (ty_rty (rty_record lt2)) ->
    Join lv1 lv2 lv -> Join lt1 lt2 lt ->
    typing_val G (val_rval (rval_record lv)) (ty_rty (rty_record lt)).
Proof.
  intros G lv lv1 lv2 lt lt1 lt2 Hval1 Hval2 HJoinv HJoint.
  generalize dependent lt2. generalize dependent lt1. generalize dependent lt.
  induction HJoinv; intros lt lt1 Hval1 lt2 Hval2 HJoint.
  - invert Hval2. apply map_eq_nil in H; subst; simpl in *.
    invert HJoint; auto.
  - invert Hval1. apply map_eq_nil in H; subst; simpl in *.
    invert HJoint; auto.
  - specialize(typing_record_cons_ex_l G l1 t1 v1 lt1 Hval1) as Hrec1. destruct Hrec1 as [t1' [lts1 Hrec1]]; subst.
    specialize(typing_record_cons_ex_l G l2 t2 v2 lt2 Hval2) as Hrec2. destruct Hrec2 as [t2' [lts2 Hrec2]]; subst.
    invert HJoint.
    + apply typing_record_cons_inv in Hval1; destruct Hval1.
      apply typing_record_cons; auto. eapply IHHJoinv; eauto.
    + apply lexico_antirefl in H; contradiction.
  - specialize(typing_record_cons_ex_l G l1 t1 v1 lt1 Hval1) as Hrec1. destruct Hrec1 as [t1' [lts1 Hrec1]]; subst.
    specialize(typing_record_cons_ex_l G l2 t2 v2 lt2 Hval2) as Hrec2. destruct Hrec2 as [t2' [lts2 Hrec2]]; subst.
    invert HJoint.
    + apply lexico_antirefl in H; contradiction.
    + apply typing_record_cons_inv in Hval2; destruct Hval2.
      apply typing_record_cons; auto. eapply IHHJoinv; eauto.
Qed.

Definition val_rec := list (String.string * value).
Definition val_stringlist (l:val_rec) := (val_rval (rval_record l)).
Coercion val_stringlist : val_rec >-> value.

Definition ty_rec := list (String.string * ty).
Definition ty_stringlist (l:ty_rec) := (ty_rty (rty_record l)).
Coercion ty_stringlist : ty_rec >-> ty.

(* Lemma sr_frame : forall ins (v v' v'':val_rec) v1 v2 G (ty ty' ty'' ty1 ty2:ty_rec), *)
(*   eval_instr ins v v' -> *)
(*   typing_val G v ty -> typing_val G v' ty' -> typing_val G v'' ty'' -> *)
(*   Join v v'' v1 -> Join v' v'' v2 -> *)
(*   Join ty ty'' ty1 -> Join ty' ty'' ty2 -> *)
(*   typing_instr G ins ty1 ty2 -> *)
(*   typing_instr G ins ty ty'. *)
(* Proof. *)
(*   intros ins v v' v'' v1 v2 G ty ty' ty'' ty1 ty2 Heval Hty Hty' Hty'' HJoinv1 HJoinv2 HJoint1 HJoint2 Hins. *)
(* Qed. *)

(** Subject reduction for instructions *)
Theorem sr_instr : forall ins G E v v' ty ty',
    typing_instr G ins ty ty' ->
    typing_val G v ty ->
    eval_instr E ins v v' ->
    typing_val G v' ty'.
Proof.
  intros instr G E v v' ty ty' Hins Hval Heval.
  generalize dependent v'; generalize dependent v.
  dependent induction Hins; intros v Hval v' Heval.
  - (* frame *)
    destruct rty_5 as [ls]; destruct rty' as [ls']; destruct rty'' as [ls''];
      destruct rty1 as [ls1]; destruct rty2 as [ls2].
    (* destruct v. *)
    (* specialize (join_type_value g5 v ls1 ls ls'' Hval H1) as [lv1 [lv2 [HJoin [Htlv1 Htlv2]]]]. *)
    (* specialize (IHHins (val_record (rval_record lv1)) Htlv1). *)
    admit.
  - (* noop *)
    dependent induction Heval; subst.
    + destruct rval_5 as [l]; destruct rval' as [l']; destruct rval'' as [l''];
        destruct rval1 as [l1]; destruct rval2 as [l2].
      apply typing_record_nil_inv_r in Hval; subst.
      apply Join_nil in H; destruct H; subst.
      specialize (IHHeval (typing_record_nil g5) eq_refl).
      apply typing_record_nil_inv_r in IHHeval; subst.
      invert H0; apply typing_record_nil.
    + assumption.
  - (* seq *)
    clear Hins1 Hins2.
    generalize dependent ty_3; generalize dependent ty_2; generalize dependent ty_1.
    dependent induction Heval; intros ty_1 Hval ty_2 IHHIns ty_3 IHHIns2.
    + rename instruction5 into ins1; rename instruction' into ins2.
      assert (eval_instr E5 (instr_seq ins1 ins2) (val_rval rval1) (val_rval rval2)) as Heval'.
      { eapply eval_instr_frame; eauto. }
      destruct rval_5 as [l1']; destruct rval' as [l3']; destruct rval'' as [l''];
        destruct rval1 as [l1]; destruct rval2 as [l3].
      specialize (IHHeval ins1 ins2 eq_refl).
      admit.
    + eauto.
  - (* assign *)
    generalize dependent b; generalize dependent a.
    dependent induction Heval; subst; intros a Hval b Hrhs Hlhs.
    + destruct rval_5 as [l]; destruct rval' as [l']; destruct rval'' as [l''];
        destruct rval1 as [l1]; destruct rval2 as [l2].
      specialize (IHHeval lhs5 rhs5 eq_refl).
      admit.
    + eapply sr_lhs; eauto. eapply sr_rhs; eauto.
Admitted.
