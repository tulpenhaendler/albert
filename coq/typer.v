(**                     Type-checker for albert                                *)

Require Import aux albert error label_lexico lexico_sort.
Require Import pretty_printers.
Require Import Coq.Program.Equality.
Require Import List String Permutation Bool.
Require ZArith.

Theorem wf_eq_type : forall G ty ty',
    typing_type_eq G ty ty' ->
    (typing_ty_well_formed G ty <-> typing_ty_well_formed G ty').
Proof.
  (* Prooved in another git branch, this is a placeholder *)
Admitted.

Lemma type_val_wf_ty : forall G val ty,
    typing_val G val ty ->
    typing_ty_well_formed G ty.
Proof.
  (* Prooved on another branch, placeholder *)
Admitted.

Ltac invert H :=
  inversion H; clear H; subst.

(* First off, a few useful lemmas about record typing *)

Lemma typing_record_nil : forall G,
    typing_val G (val_rval (rval_record nil)) (ty_rty (rty_record nil)).
Proof.
  intros G.
  assert ((map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__)) nil) = nil) as Hrew1 by reflexivity.
  assert ((map (fun pat_ : label * value * label * ty * ty =>
              let (p, _) := pat_ in let (p0, ty'__) := p in let (p1, l'__) := p0 in let (_, _) := p1 in (l'__, ty'__)) nil) = nil) as Hrew2 by reflexivity.
  rewrite <- Hrew1 at 1; rewrite <- Hrew2.
  constructor; simpl; intuition.
Qed.

Lemma typing_record_nil_inv_r : forall G l1,
    typing_val G (val_rval (rval_record l1)) (ty_rty (rty_record nil)) ->
    l1 = nil.
Proof.
  intros G l1 Hval.
  invert Hval.
  apply map_eq_nil in H1; subst; reflexivity.
Qed.

Lemma typing_record_nil_inv_l : forall G l1,
    typing_val G (val_rval (rval_record nil)) (ty_rty (rty_record l1)) ->
    l1 = nil.
Proof.
  intros G l1 Hval.
  invert Hval.
  apply map_eq_nil in H; subst; reflexivity.
Qed.

Lemma typing_record_cons_ex_l : forall G l1 v1 lv lt,
    typing_val G (val_rval (rval_record ((l1, v1)::lv))) (ty_rty (rty_record lt)) ->
    exists t1 lts, lt = (l1, t1)::lts.
Proof.
  intros G l1 v1 lv lt Hval.
  invert Hval.
  destruct l__val__ty__list; simpl in *.
  - discriminate H.
  - destruct p as [[l v] t]. invert H.
    exists t. exists (map (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__)) l__val__ty__list). reflexivity.
Qed.

Lemma typing_record_cons_ex_r : forall G l1 t1 lv lt,
    typing_val G (val_rval (rval_record lv)) (ty_rty (rty_record ((l1, t1)::lt))) ->
    exists v1 lvs, lv = (l1, v1)::lvs.
Proof.
  intros G l1 t1 lv lt Hval.
  invert Hval.
  destruct l__val__ty__list; simpl in *.
  - discriminate H1.
  - destruct p as [[l v] t]. invert H1.
    exists v. exists (map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__))
                l__val__ty__list). reflexivity.
Qed.

Lemma typing_record_cons : forall G l1 v1 t1 lv lt,
    typing_val G v1 t1 ->
    typing_val G (val_rval (rval_record lv)) (ty_rty (rty_record lt)) ->
    typing_val G (val_rval (rval_record ((l1, v1)::lv))) (ty_rty (rty_record ((l1, t1)::lt))).
Proof.
  intros G l1 v1 t1 lv lt Hval1 Hvals.
  invert Hvals.
  assert ((l1, v1) :: map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__)) l__val__ty__list =
         map (fun pat_ : label * value * ty => let (p, _) := pat_ in let (l__, val__) := p in (l__, val__)) ((l1,v1,t1)::l__val__ty__list)) as Hrew1 by reflexivity.
  assert ((l1, t1) :: map (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__)) l__val__ty__list =
          map (fun pat_ : label * value * ty => let (p, ty__) := pat_ in let (l__, _) := p in (l__, ty__)) ((l1,v1,t1)::l__val__ty__list)) as Hrew2 by reflexivity.
  rewrite Hrew1; rewrite Hrew2.
  constructor; simpl.
  intros val__ ty__ [HEq|HIn]; auto. invert HEq; assumption.
Qed.

Lemma typing_record_cons_inv : forall G l1 v1 t1 lv lt,
    typing_val G (val_rval (rval_record ((l1, v1)::lv))) (ty_rty (rty_record ((l1, t1)::lt))) ->
    (typing_val G v1 t1 /\ typing_val G (val_rval (rval_record lv)) (ty_rty (rty_record lt))).
Proof.
  intros G l1 v1 t1 lv lt Hval.
  invert Hval.
  assert (exists ls, l__val__ty__list = (l1, v1, t1)::ls) as Heqls.
  { destruct l__val__ty__list. inversion H. simpl in *. destruct p as [[l1' v1'] t1'].
    invert H. invert H1. exists l__val__ty__list; reflexivity. }
  destruct Heqls as [ls Heqls]. split; subst; simpl in *.
  - apply H2; auto.
  - invert H; invert H1.
    constructor. intros val__ ty__ HIn. apply H2; auto.
Qed.

Corollary val_record_nil_invert : forall G var val5 ty',
    typing_val G (val_rval (rval_record ((var, val5)::nil))) (ty_rty (rty_record ((var, ty')::nil))) ->
    typing_val G val5 ty'.
Proof.
  intros G var val5 ty' Hval.
  specialize (typing_record_cons_inv G var val5 ty' nil nil Hval) as [H _].
  assumption.
Qed.

(** Better induction principle on values *)
Section val_ind.
  Variable P : value -> Prop.

  Hypothesis Hnum : forall v, P (val_num v).
  Hypothesis Hbool : forall b, P (val_bool b).
  Hypothesis Hstring : forall s, P (val_string s).
  Hypothesis Hbytes : forall b, P (val_bytes b).
  Hypothesis Hrval : forall lv,
      Forall P (map (fun '(_,v) => v) lv) ->
      P (val_rval (rval_record lv)).
  Hypothesis Hcons : forall s v vt, P v -> P (val_constr s v vt).
  Hypothesis Hlist : forall lv ty,
      Forall P lv ->
      P (val_lval (lval_list lv ty)).
  Hypothesis Hmap : forall lv ty1 ty2,
      Forall (fun '(vl, vr) => P vl /\ P vr) lv ->
      P (val_mval (mval_map lv ty1 ty2)).
  Hypothesis Hadd : forall a, P (val_add a).
  Hypothesis Hkey : forall k, P (val_key k).
  Hypothesis Hsig : forall s, P (val_signature s).

  Fixpoint val_ind (v : value) : (P v) :=
    match v as v0 return (P v0) with
    | val_rval rval5 =>
      match rval5 as r return (P (val_rval r)) with
      | rval_record l =>
        Hrval l
              (list_ind (fun l0 : list (label * value) => Forall P (map (fun '(_, v0) => v0) l0)) (Forall_nil P)
                        (fun (a : label * value) (l0 : list (label * value)) (IHl : Forall P (map (fun '(_, v0) => v0) l0)) =>
                           Forall_cons (let '(_, v0) := a in v0) (let (_, v0) as p return (P (let '(_, v0) := p in v0)) := a in val_ind v0) IHl) l)
      end
    | val_num num_val5 => Hnum num_val5
    | val_constr s v vt => Hcons s v vt (val_ind v)
    | val_bool b => Hbool b
    | val_string s => Hstring s
    | val_bytes b => Hbytes b
    | val_lval lval5 =>
      match lval5 as l return (P (val_lval l)) with
      | lval_list l ty =>
        Hlist l ty (list_ind (fun l0 : list value => Forall P l0) (Forall_nil P)
                             (fun (a : value) (l0 : list value) (IHl : Forall P l0) =>
                                Forall_cons a (val_ind a) IHl) l)
      end
    | val_mval mval5 =>
      match mval5 as l return (P (val_mval l)) with
      | mval_map m ty ty' =>
        Hmap m ty ty' (list_ind (fun l0 : list (value * value) => Forall (fun '(vl, vr) => P vl /\ P vr) l0) (Forall_nil (fun '(vl, vr) => P vl /\ P vr))
                      (fun (a : value * value) (l0 : list (value * value)) (IHl : Forall (fun '(vl, vr) => P vl /\ P vr) l0) =>
                         (let '(v1, v2) := a in Forall_cons (v1, v2) (conj (val_ind v1) (val_ind v2)) IHl)) m)
      end
    | val_add a => (Hadd a)
    | val_key k => (Hkey k)
    | val_signature s => (Hsig s)
  end.
End val_ind.

Section ty_ind.
  Variable P : ty -> Prop.

  Hypothesis Hnat : P (ty_nat).
  Hypothesis Hint : P (ty_int).
  Hypothesis Htimestamp : P (ty_timestamp).
  Hypothesis Hmutez : P (ty_mutez).
  Hypothesis Hbool : P (ty_bool).
  Hypothesis Hunit : P (ty_unit).
  Hypothesis Hvar : forall v, P (ty_var v).
  Hypothesis Hstring : P (ty_string).
  Hypothesis Hbytes : P (ty_bytes).
  Hypothesis Hrty : forall lt,
      Forall P (map (fun '(_,t) => t) lt) ->
      P (ty_rty (rty_record lt)).
  Hypothesis Hprod : forall ty1 ty2,
      P ty1 -> P ty2 -> P (ty_prod ty1 ty2).
  Hypothesis Hvty : forall lt,
      Forall P (map (fun '(_,t) => t) lt) ->
      P (ty_vty (vty_variant lt)).
  Hypothesis Hoption : forall ty,
      P ty -> P (ty_option ty).
  Hypothesis Hor : forall ty1 ty2,
      P ty1 -> P ty2 -> P (ty_or ty1 ty2).
  Hypothesis Hlist : forall ty,
      P ty -> P (ty_list ty).
  Hypothesis Hmap : forall ty ty',
      P ty -> P ty' -> P (ty_map ty ty').
  Hypothesis Hbigmap : forall ty ty',
      P ty -> P ty' -> P (ty_big_map ty ty').
  Hypothesis Hoperation : P ty_operation.
  Hypothesis Hkeyhash : P ty_key_hash.
  Hypothesis Haddress : P ty_address.
  Hypothesis Hcontract : forall ty,
      P ty -> P (ty_contract ty).
  Hypothesis Hkey : P ty_key.
  Hypothesis Hsig : P ty_signature.

  Fixpoint ty_ind (t : ty) : (P t) :=
    match t as t0 return (P t0) with
    | ty_rty rty5 =>
      match rty5 as r return (P (ty_rty r)) with
      | rty_record l =>
        Hrty l
              (list_ind (fun l0 : list (label * ty) => Forall P (map (fun '(_, t0) => t0) l0)) (Forall_nil P)
                        (fun (a : label * ty) (l0 : list (label * ty)) (IHl : Forall P (map (fun '(_, v0) => v0) l0)) =>
                           Forall_cons (let '(_, t0) := a in t0) (let (_, t0) as p return (P (let '(_, t0) := p in t0)) := a in ty_ind t0) IHl) l)
      end
    | ty_var v => Hvar v
    | ty_nat => Hnat
    | ty_int => Hint
    | ty_timestamp => Htimestamp
    | ty_mutez => Hmutez
    | ty_bool => Hbool
    | ty_unit => Hunit
    | ty_string => Hstring
    | ty_bytes => Hbytes
    | ty_prod ty1 ty2 => Hprod ty1 ty2 (ty_ind ty1) (ty_ind ty2)
    | ty_vty vty5 =>
      match vty5 as v return (P (ty_vty v)) with
      | vty_variant l =>
        Hvty l
              (list_ind (fun l0 : list (label * ty) => Forall P (map (fun '(_, t0) => t0) l0)) (Forall_nil P)
                        (fun (a : label * ty) (l0 : list (label * ty)) (IHl : Forall P (map (fun '(_, v0) => v0) l0)) =>
                           Forall_cons (let '(_, t0) := a in t0) (let (_, t0) as p return (P (let '(_, t0) := p in t0)) := a in ty_ind t0) IHl) l)
      end
    | ty_option ty => Hoption ty (ty_ind ty)
    | ty_or ty1 ty2 => Hor ty1 ty2 (ty_ind ty1) (ty_ind ty2)
    | ty_list ty => Hlist ty (ty_ind ty)
    | ty_map ty ty' => Hmap ty ty' (ty_ind ty) (ty_ind ty')
    | ty_big_map ty ty' => Hbigmap ty ty' (ty_ind ty) (ty_ind ty')
    | ty_operation => Hoperation
    | ty_key_hash => Hkeyhash
    | ty_address => Haddress
    | ty_contract ty => Hcontract ty (ty_ind ty)
    | ty_key => Hkey
    | ty_signature => Hsig
  end.
End ty_ind.

(* Get rid of the type variables *)

(** A type doesn't contain any variable *)
Inductive type_without_var : ty -> Prop :=
| nat_without_var : type_without_var ty_nat
| int_without_var : type_without_var ty_int
| timestamp_without_var : type_without_var ty_timestamp
| mutez_without_var : type_without_var ty_mutez
| unit_without_var : type_without_var ty_unit
| string_without_var : type_without_var ty_string
| bytes_without_var : type_without_var ty_bytes
| record_without_var : forall lt,
    Forall type_without_var (map (fun '(_, t) => t) lt) ->
    type_without_var (ty_rty (rty_record lt))
| prod_without_var : forall ty1 ty2,
    type_without_var ty1 -> type_without_var ty2 ->
    type_without_var (ty_prod ty1 ty2)
| variant_without_var : forall lt,
    Forall type_without_var (map (fun '(_, t) => t) lt) ->
    type_without_var (ty_vty (vty_variant lt))
| option_without_var : forall ty,
    type_without_var ty ->
    type_without_var (ty_option ty)
| or_without_var : forall ty1 ty2,
    type_without_var ty1 -> type_without_var ty2 ->
    type_without_var (ty_or ty1 ty2)
| bool_without_var : type_without_var ty_bool
| list_without_var : forall ty,
    type_without_var ty ->
    type_without_var (ty_list ty)
| map_without_var : forall ty ty',
    type_without_var ty -> type_without_var ty' ->
    type_without_var (ty_map ty ty')
| bigmap_without_var : forall ty ty',
    type_without_var ty -> type_without_var ty' ->
    type_without_var (ty_big_map ty ty')
| operation_without_var : type_without_var ty_operation
| key_hash_without_var : type_without_var ty_key_hash
| address_without_var : type_without_var ty_address
| contract_without_var : forall ty,
    type_without_var ty ->
    type_without_var (ty_contract ty)
| key_without_var : type_without_var ty_key
| sig_without_var : type_without_var ty_signature.

Open Scope string_scope.


(** The types declared inside an env don't contain any variable *)
Inductive env_without_var : g -> Prop :=
| empty_without_var : env_without_var g_empty
| typedef_without_var : forall env v t,
    env_without_var env ->
    type_without_var t ->
    env_without_var (g_typedef env v t)
| fundecl_without_var : forall env v t1 t2,
    env_without_var env ->
    type_without_var t1 -> type_without_var t2 ->
    env_without_var (g_fundecl env v t1 t2).
Open Scope monadic_let_scope.

(** Get a full type (without type variables from a type and an env
    Also sorts fields of records and constructors of variants *)
Fixpoint full_type_from_env (t:ty) := fix ftfr (env:g) :=
  match t with
    | ty_var v =>
      match env with
      | g_empty => Failed _ (Not_found v)
      | g_typedef env' v' t' => if (eqb_string v v') then Return _ t' else ftfr env'
      | g_fundecl env' _ _ _ => ftfr env'
      end
    | ty_rty (rty_record lt) =>
      let%err lt' :=(fix full_type_from_rty lt :=
                       match lt with
                       | nil => Return _ nil
                       | (l, t)::tl => let%err t' := full_type_from_env t env in
                                     let%err tl' := full_type_from_rty tl in
                                     Return _ ((l, t')::tl')
                       end) lt in
      Return _ (ty_rty (rty_record lt'))
    | ty_prod ty1 ty2 =>
      let%err ty1' := full_type_from_env ty1 env in
      let%err ty2' := full_type_from_env ty2 env in
      Return _ (ty_prod ty1' ty2')
    | ty_vty (vty_variant lt) =>
      let%err lt' := (fix full_type_from_vty lt :=
                        match lt with
                        | nil => Return _ nil
                        | (l, t)::tl => let%err t' := full_type_from_env t env in
                                      let%err tl' := full_type_from_vty tl in
                                      Return _ ((l, t')::tl')
                        end) lt in
      Return _ (ty_vty (vty_variant lt'))
    | ty_option ty =>
      let%err ty' := full_type_from_env ty env in
      Return _ (ty_option ty')
    | ty_or ty1 ty2 =>
      let%err ty1' := full_type_from_env ty1 env in
      let%err ty2' := full_type_from_env ty2 env in
      Return _ (ty_or ty1' ty2')
    | ty_list ty =>
      let%err ty' := full_type_from_env ty env in
      Return _ (ty_list ty')
    | ty_map ty1 ty2 =>
      let%err ty1' := full_type_from_env ty1 env in
      let%err ty2' := full_type_from_env ty2 env in
      Return _ (ty_map ty1' ty2')
    | ty_big_map ty1 ty2 =>
      let%err ty1' := full_type_from_env ty1 env in
      let%err ty2' := full_type_from_env ty2 env in
      Return _ (ty_big_map ty1' ty2')
    | ty_contract ty =>
      let%err ty' := full_type_from_env ty env in
      Return _ (ty_contract ty')
    | _ => Return _ t
  end.

Lemma Forall_perm {A} : forall (l1 l2: list A) P,
    Permutation l1 l2 ->
    Forall P l1 <-> Forall P l2.
Proof.
  intros l1 l2 P HPerm.
  split; intro HForall;
    rewrite Forall_forall in *; intros x HIn;
      apply HForall.
  - apply Permutation_in with l2; auto. apply Permutation_sym; auto.
  - apply Permutation_in with l1; auto.
Qed.

Lemma full_type_from_env_without_var : forall t G t',
    env_without_var G ->
    full_type_from_env t G = Return _ t' ->
    type_without_var t'.
Proof.
  induction t using ty_ind; simpl; intros G t' Henv Hret;
    try (destruct G; invert Hret; constructor).
  - (* ty_var *)
    induction G.
    + invert Hret.
    + invert Henv; auto.
    + invert Henv. specialize (IHG H1).
      destruct (eqb_string v tvar5).
      * invert Hret; auto.
      * auto.
  - (* ty_rty *)
    destruct G; apply bind_eq_return in Hret as [lt' [Hft Hret]]; invert Hret;
      generalize dependent lt'; induction lt; intros lt' Hft;
        try (invert Hft; constructor; simpl; constructor);
        try (destruct a as [l t]; apply bind_eq_return in Hft as [t' [Hft Hret]];
             apply bind_eq_return in Hret as [lt'' [Htl Hret]]; invert Hret;
             invert H; specialize (IHlt H3 lt'' Htl);
             constructor; simpl; constructor;
             try (eapply H2; eauto);
             try (invert IHlt; assumption)).
  - (* ty_prod *)
    destruct G; apply bind_eq_return in Hret as [t'' [Hfte1 Hret]];
      apply bind_eq_return in Hret as [t''' [Hfte2 Hret]]; invert Hret;
        specialize (IHt1 _ _ Henv Hfte1); specialize (IHt2 _ _ Henv Hfte2);
          constructor; auto.
  - (* ty_vty *)
    destruct G; apply bind_eq_return in Hret as [lt' [Hft Hret]]; invert Hret;
      generalize dependent lt'; induction lt; intros lt' Hft;
        try (invert Hft; constructor; simpl; constructor);
        try (destruct a as [l t]; apply bind_eq_return in Hft as [t' [Hft Hret]];
             apply bind_eq_return in Hret as [lt'' [Htl Hret]]; invert Hret;
             invert H; specialize (IHlt H3 lt'' Htl);
             constructor; simpl; constructor;
             try (eapply H2; eauto);
             try (invert IHlt; assumption)).
  - (* ty_option *)
    destruct G; apply bind_eq_return in Hret as [t'' [Hfte Hret]]; invert Hret;
      specialize (IHt _ _ Henv Hfte); constructor; auto.
  - (* ty_or *)
    destruct G; apply bind_eq_return in Hret as [t'' [Hfte1 Hret]];
      apply bind_eq_return in Hret as [t''' [Hfte2 Hret]]; invert Hret;
        specialize (IHt1 _ _ Henv Hfte1); specialize (IHt2 _ _ Henv Hfte2);
          constructor; auto.
  - (* ty_list *)
    destruct G; apply bind_eq_return in Hret as [t'' [Hfte Hret]]; invert Hret;
      specialize (IHt _ _ Henv Hfte); constructor; auto.
  - (* ty_map *)
    destruct G; apply bind_eq_return in Hret as [t'' [Hfte1 Hret]];
      apply bind_eq_return in Hret as [t''' [Hfte2 Hret]]; invert Hret;
        specialize (IHt1 _ _ Henv Hfte1); specialize (IHt2 _ _ Henv Hfte2);
          constructor; auto.
  - (* ty_big_map *)
    destruct G; apply bind_eq_return in Hret as [t'' [Hfte1 Hret]];
      apply bind_eq_return in Hret as [t''' [Hfte2 Hret]]; invert Hret;
        specialize (IHt1 _ _ Henv Hfte1); specialize (IHt2 _ _ Henv Hfte2);
          constructor; auto.
  - (* ty_contract *)
    destruct G; apply bind_eq_return in Hret as [t'' [Hfte Hret]]; invert Hret;
      specialize (IHt _ _ Henv Hfte); constructor; auto.
Qed.

Fixpoint remove_tvar_from_val (v:value) (env:g) : M value :=
  match v with
  | val_rval (rval_record lv) =>
    let%err lv'  := (fix remove_tvar_from_rval lv :=
                       match lv with
                       | nil => Return _ nil
                       | (l, v)::tl => let%err v' := remove_tvar_from_val v env in
                                     let%err tl' := remove_tvar_from_rval tl in
                                     Return _ ((l, v')::tl')
                       end) lv in
    Return _ (val_rval (rval_record lv'))
  | val_num n => Return _ (val_num n)
  | val_bool b => Return _ (val_bool b)
  | val_string s => Return _ (val_string s)
  | val_bytes b => Return _ (val_bytes b)
  | val_constr c v vt =>
    let%err v' := remove_tvar_from_val v env in
    let%err t' := full_type_from_env (ty_vty vt) env in
    match t' with
    | ty_vty vt' => Return _ (val_constr c v' vt')
    | _ => Failed _ Should_not_happen
    end
  | val_lval (lval_list lv ty) =>
    let%err ty' := full_type_from_env ty env in
    let%err lv' := (fix remove_tvar_from_lval lv :=
                      match lv with
                      | nil => Return _ nil
                      | v::tl => let%err v' := remove_tvar_from_val v env in
                               let%err tl' := remove_tvar_from_lval tl in
                               Return _ (v'::tl')
                      end) lv in
    Return _ (val_lval (lval_list lv' ty'))
  | val_mval (mval_map lv ty1 ty2) =>
    let%err ty1' := full_type_from_env ty1 env in
    let%err ty2' := full_type_from_env ty2 env in
    let%err lv'  := (fix remove_tvar_from_mval mv :=
                       match mv with
                       | nil => Return _ nil
                       | (v1, v2)::tl => let%err v1' := remove_tvar_from_val v1 env in
                                       let%err v2' := remove_tvar_from_val v2 env in
                                       let%err tl' := remove_tvar_from_mval tl in
                                       Return _ ((v1', v2')::tl')
                       end) lv
    in Return _ (val_mval (mval_map lv' ty1' ty2'))
  | val_add a => Return _ (val_add a)
  | val_key k => Return _ (val_key k)
  | val_signature s => Return _ (val_signature s)
  end.

Definition remove_tvar_from_arg (a:arg) (env:g) : M arg :=
  match a with
  | arg_var v => Return _ (arg_var v)
  | arg_value v => let%err v' := remove_tvar_from_val v env in
                  Return _ (arg_value v')
  | arg_record r => Return _ (arg_record r)
  end.

Definition remove_tvar_from_funct (funct:f) (env:g) : M f :=
  match funct with
  | fun_var f => Return _ (fun_var f)
  | fun_dup => Return _ fun_dup
  | fun_constr c vt => let%err t' := full_type_from_env (ty_vty vt) env in
                      match t' with
                      | ty_vty vt' => Return _ (fun_constr c vt')
                      | _ => Failed _ Should_not_happen
                      end
  | fun_contract ty =>
    let%err ty' := full_type_from_env ty env in
    Return _ (fun_contract ty')
  | fun_address => Return _ fun_address
  | fun_implicit_account => Return _ fun_implicit_account
  | fun_hash_key | fun_blake2b | fun_sha256 | fun_sha512 => Return _ funct
  end.

Fixpoint remove_tvar_from_rhs (r:rhs) (env:g) : M rhs :=
  match r with
  | rhs_arg a => let%err a' := remove_tvar_from_arg a env in
                Return _ (rhs_arg a')
  | rhs_app f a => let%err a' := remove_tvar_from_arg a env in
                  let%err f' := remove_tvar_from_funct f env in
                  Return _ (rhs_app f' a')
  | rhs_projection _ _ | rhs_update _ _ => Return _ r
  | rhs_add _ _ | rhs_sub _ _ | rhs_mul _ _ | rhs_ediv _ _
  | rhs_not _ | rhs_and _ _ | rhs_or _ _ | rhs_xor _ _
  | rhs_lsl _ _ | rhs_lsr _ _
  | rhs_abs _ | rhs_is_nat _ | rhs_int _
  | rhs_concat _ _ | rhs_size _ | rhs_slice _ _ _ | rhs_bytes_pack _ => Return _ r
  | rhs_bytes_unpack t l => let%err t' := full_type_from_env t env in
                           (Return _ (rhs_bytes_unpack t' l))
  | rhs_eq _ _ | rhs_neq _ _ | rhs_inf _ _ | rhs_infeq _ _ | rhs_sup _ _ | rhs_supeq _ _ => Return _ r
  | rhs_match l branches =>
    let 'branchesrhs_branches lbr := branches in
    let%err lbr' :=(fix remove_tvar_from_branches lbr :=
                       match lbr with
                       | nil => Return _ nil
                       | (pat, r)::tl => let%err r' := remove_tvar_from_rhs r env in
                                       let%err tl' := remove_tvar_from_branches tl in
                                       Return _ ((pat, r')::tl')
                       end) lbr in
    Return _ (rhs_match l (branchesrhs_branches lbr'))
  | rhs_list _ | rhs_list_cons _ _ => Return _ r
  | rhs_list_map l1 l2 r => let%err r' := remove_tvar_from_rhs r env in
                           Return _ (rhs_list_map l1 l2 r')
  | rhs_map vars => Return _ (rhs_map vars)
  | rhs_map_map l1 l2 l3 r =>
    let%err r' := remove_tvar_from_rhs r env in
    Return _ (rhs_map_map l1 l2 l3 r')
  | rhs_map_get var l => Return _ (rhs_map_get var l)
  | rhs_map_update var l1 l2 => Return _ (rhs_map_update var l1 l2)
  | rhs_amount | rhs_check_signature _ _ _
  | rhs_self | rhs_transfer_tokens _ _ _ | rhs_set_delegate _
  | rhs_sender | rhs_source | rhs_now  => Return _ r
  end.

Fixpoint remove_tvar_from_ins (ins:instruction) (env:g) : M instruction :=
  match ins with
  | instr_noop => Return _ instr_noop
  | instr_seq ins1 ins2 =>
    let%err ins1' := remove_tvar_from_ins ins1 env in
    let%err ins2' := remove_tvar_from_ins ins2 env in
    Return _ (instr_seq ins1' ins2')
  | instr_assign lhs rhs =>
    let%err rhs' := remove_tvar_from_rhs rhs env in
    Return _ (instr_assign lhs rhs')
  | instr_drop v => Return _ (instr_drop v)
  | instr_match l branches =>
    let 'branchesins_branches lbr := branches in
    let%err lbr'  := (fix remove_tvar_from_branches lbr :=
                        match lbr with
                        | nil => Return _ nil
                        | (pat, i)::tl => let%err i' := remove_tvar_from_ins i env in
                                        let%err tl' := remove_tvar_from_branches tl in
                                        Return _ ((pat, i')::tl')
                        end) lbr in
    Return _ (instr_match l (branchesins_branches lbr'))
  | instr_loop_left v v' body =>
    let%err body' := remove_tvar_from_ins body env in
    Return _ (instr_loop_left v v' body')
  | instr_loop v body =>
    let%err body' := remove_tvar_from_ins body env in
    Return _ (instr_loop v body')
  | instr_list_for v v' body =>
    let%err body' := remove_tvar_from_ins body env in
    Return _ (instr_list_for v v' body')
  | instr_failwith a =>
    let%err a' := remove_tvar_from_arg a env in
    Return _ (instr_failwith a')
  | instr_map_for var l1 l2 ins =>
    let%err ins' := remove_tvar_from_ins ins env in
    Return _ (instr_map_for var l1 l2 ins')
end.

Fixpoint remove_tvar_from_program (l:prog) (env:g) : M (prog) :=
  match l with
  | prog_def (def_tdef tv t) =>
    let%err t' := full_type_from_env t env in
    Return _ (prog_def (def_tdef tv t'))
  | prog_def (def_fdef fv t1 t2 ins) =>
    let%err t1' := full_type_from_env t1 env in
    let%err t2' := full_type_from_env t2 env in
    let%err ins' := remove_tvar_from_ins ins env in
    Return _ (prog_def (def_fdef fv t1' t2' ins'))
  | prog_defs (def_tdef tv t) tl =>
    let%err t' := full_type_from_env t env in
    let%err tl' := remove_tvar_from_program tl (g_typedef env tv t') in
    Return _ (prog_defs (def_tdef tv t') tl')
  | prog_defs (def_fdef fv t1 t2 ins) tl =>
    let%err t1' := full_type_from_env t1 env in
    let%err t2' := full_type_from_env t2 env in
    let%err ins' := remove_tvar_from_ins ins env in
    let%err tl' := remove_tvar_from_program tl (g_fundecl env fv t1' t2') in
    Return _ (prog_defs (def_fdef fv t1' t2' ins') tl')
end.

(* Sort the record fields and the variant constructors *)

Fixpoint sort_fields_in_type (t:ty) :=
  match t with
  | ty_var v => ty_var v
  | ty_rty (rty_record lt) =>
    ty_rty (rty_record (LexicoTySort.sort (List.map (fun '(l, ty) => (l, sort_fields_in_type ty)) lt)))
  | ty_prod ty1 ty2 => ty_prod (sort_fields_in_type ty1) (sort_fields_in_type ty2)
  | ty_vty (vty_variant lt) =>
    ty_vty (vty_variant (LexicoTySort.sort (List.map (fun '(l, ty) => (l, sort_fields_in_type ty)) lt)))
  | ty_option ty => ty_option (sort_fields_in_type ty)
  | ty_or ty1 ty2 => ty_or (sort_fields_in_type ty1) (sort_fields_in_type ty2)
  | ty_list ty => ty_list (sort_fields_in_type ty)
  | ty_map ty1 ty2 => ty_map (sort_fields_in_type ty1) (sort_fields_in_type ty2)
  | ty_big_map ty1 ty2 => ty_big_map (sort_fields_in_type ty1) (sort_fields_in_type ty2)
  | ty_contract ty => ty_contract (sort_fields_in_type ty)
  | _ => t
  end.

Fixpoint sort_fields_in_val (v:value) : value :=
  match v with
  | val_rval (rval_record lv) =>
    val_rval (rval_record (LexicoValSort.sort (List.map (fun '(l, v) => (l, sort_fields_in_val v)) lv)))
  | val_constr c v (vty_variant vt) =>
    val_constr c (sort_fields_in_val v) (vty_variant (LexicoTySort.sort (List.map (fun '(l, ty) => (l, sort_fields_in_type ty)) vt)))
  | val_lval (lval_list lv ty) =>
    val_lval (lval_list (List.map sort_fields_in_val lv) ty)
  | val_mval (mval_map lv ty1 ty2) =>
    val_mval (mval_map (List.map (fun '(v1, v2) => (sort_fields_in_val v1, sort_fields_in_val v2)) lv) ty1 ty2)
  | _ => v
  end.

Definition sort_fields_in_arg (a:arg) : arg :=
  match a with
  | arg_var v => arg_var v
  | arg_value v => arg_value (sort_fields_in_val v)
  | arg_record r => arg_record (LexicoLabSort.sort r)
  end.

Definition sort_fields_in_funct (funct:f) : f :=
  match funct with
  | fun_var f => fun_var f
  | fun_dup => fun_dup
  | fun_constr c (vty_variant vt) =>
    fun_constr c (vty_variant (LexicoTySort.sort (List.map (fun '(l, ty) => (l, sort_fields_in_type ty)) vt)))
  | fun_contract ty => fun_contract (sort_fields_in_type ty)
  | fun_address | fun_implicit_account
  | fun_hash_key | fun_blake2b | fun_sha256 | fun_sha512 => funct
  end.

Fixpoint sort_fields_in_rhs (r:rhs) : rhs :=
  match r with
  | rhs_arg a => rhs_arg (sort_fields_in_arg a)
  | rhs_app f a => rhs_app (sort_fields_in_funct f) (sort_fields_in_arg a)
  | rhs_projection _ _ => r
  | rhs_update l labs => rhs_update l (LexicoLabSort.sort labs)
  | rhs_add _ _ | rhs_sub _ _ | rhs_mul _ _ | rhs_ediv _ _
  | rhs_not _ | rhs_and _ _ | rhs_or _ _ | rhs_xor _ _
  | rhs_lsl _ _ | rhs_lsr _ _
  | rhs_abs _ | rhs_is_nat _ | rhs_int _
  | rhs_concat _ _ | rhs_size _ | rhs_slice _ _ _ | rhs_bytes_pack _ => r
  | rhs_bytes_unpack t l => rhs_bytes_unpack (sort_fields_in_type t) l
  | rhs_eq _ _ | rhs_neq _ _ | rhs_inf _ _ | rhs_infeq _ _ | rhs_sup _ _ | rhs_supeq _ _ => r
  | rhs_match l (branchesrhs_branches branches) =>
    rhs_match l (branchesrhs_branches (LexicoPatternRHS_Sort.sort(List.map (fun '(pat, r) => (pat, sort_fields_in_rhs r)) branches)))
  | rhs_list _ | rhs_list_cons _ _ => r
  | rhs_list_map l1 l2 r => rhs_list_map l1 l2 (sort_fields_in_rhs r)
  | rhs_map vars => rhs_map vars
  | rhs_map_map l1 l2 l3 r => rhs_map_map l1 l2 l3 (sort_fields_in_rhs r)
  | rhs_map_get _ _ | rhs_map_update _ _ _ => r
  | rhs_amount | rhs_check_signature _ _ _
  | rhs_self | rhs_transfer_tokens _ _ _ | rhs_set_delegate _
  | rhs_sender | rhs_source | rhs_now => r
  end.

Definition sort_fields_in_lhs (l:lhs) : lhs :=
  match l with
  | lhs_var v => lhs_var v
  | lhs_record ll => lhs_record (LexicoLabSort.sort ll)
  end.

Fixpoint sort_fields_in_ins (ins:instruction) : instruction :=
  match ins with
  | instr_noop => instr_noop
  | instr_seq ins1 ins2 =>
    instr_seq (sort_fields_in_ins ins1) (sort_fields_in_ins ins2)
  | instr_assign lhs rhs =>
    instr_assign (sort_fields_in_lhs lhs) (sort_fields_in_rhs rhs)
  | instr_drop v => (instr_drop v)
  | instr_match l (branchesins_branches branches) =>
    instr_match l (branchesins_branches
                     (LexicoPatternIns_Sort.sort
                        (List.map (fun '(pat, i) => (pat, sort_fields_in_ins i)) branches)))
  | instr_loop_left v v' body => instr_loop_left v v' (sort_fields_in_ins body)
  | instr_loop v body => instr_loop v (sort_fields_in_ins body)
  | instr_list_for v v' body => instr_list_for v v' (sort_fields_in_ins body)
  | instr_failwith a => instr_failwith (sort_fields_in_arg a)
  | instr_map_for var l1 l2 body => instr_map_for var l1 l2 (sort_fields_in_ins body)
  end.

Fixpoint sort_fields_in_program (l:prog) : prog :=
  match l with
  | prog_def (def_tdef tv t) => prog_def (def_tdef tv (sort_fields_in_type t))
  | prog_def (def_fdef fv t1 t2 ins) =>
    prog_def (def_fdef fv (sort_fields_in_type t1) (sort_fields_in_type t2) (sort_fields_in_ins ins))
  | prog_defs (def_tdef tv t) tl =>
    prog_defs (def_tdef tv (sort_fields_in_type t)) (sort_fields_in_program tl)
  | prog_defs (def_fdef fv t1 t2 ins) tl =>
    prog_defs (def_fdef fv (sort_fields_in_type t1) (sort_fields_in_type t2) (sort_fields_in_ins ins))
             (sort_fields_in_program tl)
  end.

Open Scope string.

(** Equivalence of type.
    Assumes that the two types are well formed *)
Fixpoint type_eq (ty1 ty2: ty) : bool :=
  match ty1, ty2 with
  | ty_rty (rty_record lt1), ty_rty (rty_record lt2) =>
    (fix rty_eq lt1 lt2 : bool :=
       match lt1, lt2 with
       | nil, nil => true
       | _, nil => false
       | nil, _ => false
       | (l1, ty1)::tl1, (l2, ty2)::tl2 => (andb (andb (eqb_string l1 l2) (type_eq ty1 ty2)) (rty_eq tl1 tl2))%bool
       end) lt1 lt2
  | ty_int, ty_int => true
  | ty_nat, ty_nat => true
  | ty_mutez, ty_mutez => true
  | ty_timestamp, ty_timestamp => true
  | ty_string, ty_string => true
  | ty_bytes, ty_bytes => true
  | ty_unit, ty_unit => true
  | ty_unit, ty_rty (rty_record nil) | ty_rty (rty_record nil), ty_unit => true
  | ty_prod ty1 ty2, ty_prod ty1' ty2' => (type_eq ty1 ty1') && (type_eq ty2 ty2')
  | ty_prod ty1 ty2, ty_rty (rty_record ((scar, ty1')::(scdr, ty2')::nil))
  | ty_rty (rty_record ((scar, ty1)::(scdr, ty2)::nil)), ty_prod ty1' ty2'
    => (eqb_string scar "car") && (eqb_string scdr "cdr") && (type_eq ty1 ty1') && (type_eq ty2 ty2')
  | ty_vty (vty_variant lt1), ty_vty (vty_variant lt2) =>
    (fix vty_eq lt1 lt2 : bool :=
      match lt1, lt2 with
       | nil, nil => true
       | _, nil => false
       | nil, _ => false
       | (l1, ty1)::tl1, (l2, ty2)::tl2 => (andb (andb (eqb_string l1 l2) (type_eq ty1 ty2)) (vty_eq tl1 tl2))%bool
       end) lt1 lt2
  | ty_option ty1, ty_option ty1' => type_eq ty1 ty1'
  | ty_option ty1, ty_vty (vty_variant ((snone, ty_unit)::(ssome, ty1')::nil))
  | ty_vty (vty_variant ((snone, ty_unit)::(ssome, ty1)::nil)), ty_option ty1' =>
    (eqb_string snone "None") && (eqb_string ssome "Some") && (type_eq ty1 ty1')
  | ty_or ty1 ty2, ty_or ty1' ty2' => (type_eq ty1 ty1') && (type_eq ty2 ty2')
  | ty_or ty1 ty2, ty_vty (vty_variant ((sleft, ty1')::(sright, ty2')::nil))
  | ty_vty (vty_variant ((sleft, ty1)::(sright, ty2)::nil)), ty_or ty1' ty2' =>
    (eqb_string sleft "Left") && (eqb_string sright "Right") && (type_eq ty1 ty1') && (type_eq ty2 ty2')
  | ty_bool, ty_bool => true
  | ty_vty (vty_variant ((sfalse, ty_unit)::(strue, ty_unit)::nil)), ty_bool
  | ty_bool, ty_vty (vty_variant ((sfalse, ty_unit)::(strue, ty_unit)::nil)) =>
    (eqb_string sfalse "False") && (eqb_string strue "True")
  | ty_list t1, ty_list t2 => (type_eq t1 t2)
  | ty_map t1 t2, ty_map t1' t2' => (type_eq t1 t1') && (type_eq t2 t2')
  | ty_operation, ty_operation => true
  | ty_key_hash, ty_key_hash => true
  | ty_address, ty_address => true
  | ty_contract ty1, ty_contract ty2 => (type_eq ty1 ty2)
  | ty_key, ty_key => true
  | ty_signature, ty_signature => true
  | _, _ => false
  end.

Definition rty_eq (rty1 rty2: rty) : bool := type_eq (ty_rty rty1) (ty_rty rty2).

Definition is_bool ty := type_eq ty ty_bool.

Lemma type_eq_correct : forall G ty1 ty2,
    type_eq ty1 ty2 = true ->
    typing_type_eq G ty1 ty2.
Proof.
  (* induction ty1; intros ty2 Heq; simpl in *; *)
  (* try (destruct ty2; simpl; invert Heq; apply typing_Teq_refl). *)
  (* - (* ty1 = record *) *)
  (*   destruct rty5 as [lt]; destruct lt. *)
  (*   + destruct ty2; try discriminate Heq. *)
  (*     * destruct rty5. destruct l. constructor. discriminate Heq. *)
  (*     * apply typing_Teq_sym. apply typing_Teq_unit. *)
  (*   + destruct p. *)
  (*     admit. *)
  (* - (* ty1 = unit *) *)
  (*   destruct ty2; try discriminate Heq. *)
  (*   + destruct rty5. destruct l; try discriminate Heq. apply typing_Teq_unit. *)
  (*   + apply typing_Teq_refl. *)
  (* - (* ty1 = prod *) *)
  (*   destruct ty2; try discriminate Heq. *)
  (*   + destruct rty5. admit. *)
  (*   + apply andb_prop in Heq. destruct Heq as [Heq1 Heq2]. *)
  (*     apply IHty1_1 in Heq1; apply IHty1_2 in Heq2. *)
  (*     admit. *)
  (* - (* ty1 = variant *) *)
  (*   destruct vty5 as [[|[l1 t1] lt1]]. *)
  (*   + destruct ty2; try discriminate Heq. *)
  (*     destruct vty5. destruct l; try discriminate Heq. *)
  (*     apply typing_Teq_refl. *)
  (*   + admit. *)
  (* - (* ty1 = option *) *)
  (*   admit. *)
  (* - (* ty1 = or *) *)
  (*   admit. *)
  (* - (* ty1 = bool *) *)
  (*   destruct ty2; try discriminate Heq. *)
  (*   + admit. *)
  (*   + apply typing_Teq_refl. *)
Admitted.

Lemma type_eq_complete : forall G ty1 ty2,
    typing_type_eq G ty1 ty2 ->
    type_eq ty1 ty2 = true.
Proof.
  (* TODO *)
Admitted.

Theorem type_eq_equiv : forall G ty1 ty2,
    typing_type_eq G ty1 ty2 <->
    type_eq ty1 ty2 = true.
Proof.
  split.
  - apply type_eq_complete.
  - apply type_eq_correct.
Qed.

Theorem type_eq_val : forall G v ty1 ty2,
    typing_type_eq G ty1 ty2 ->
    (typing_val G v ty1 <-> typing_val G v ty2).
Proof.
  (* TODO *)
Admitted.

Fixpoint type_and_remove (x : label) (l:list (label * ty)) : M (ty * list (label * ty)) :=
  match l with
  | nil => Failed _ (Not_found x)
  | (hd, t)::tl => if string_dec x hd then Return _ (t, tl)
                 else let%err (t', tl) := type_and_remove x tl in
                      Return _ (t', (hd, t)::tl)
  end.

Lemma type_and_remove_in_return: forall x l ty,
    In (x, ty) l ->
    Sorted.StronglySorted lexico_lt (map (fun '(l, _) => l) l) ->
    exists l', type_and_remove x l = Return _ (ty, l').
Proof.
  induction l; intros ty HIn HSort.
  - invert HIn.
  - destruct a as [x' ty']; simpl.
    destruct string_dec; subst.
    + destruct HIn.
      * invert H. eexists; eauto.
      * exfalso. invert HSort. rewrite Forall_forall in H3.
        assert (In x' (map (fun '(l,_) => l) l)) as HIn.
        { clear IHl H2 H3. induction l; simpl in *. contradiction.
          destruct a as [x'' _lt]. destruct H; auto. invert H. auto. }
        specialize (H3 x' HIn). apply lexico_irrefl in H3; auto.
    + destruct HIn; try (invert H; contradiction). invert HSort.
      specialize (IHl ty H H2) as [l' Hl']. rewrite Hl'; simpl.
      eexists; eauto.
Qed.

Lemma type_and_remove_return_in: forall x l ty l',
    type_and_remove x l = Return _ (ty, l') ->
    In (x, ty) l.
Proof.
  intros x l.
  induction l; intros ty l' Hret; simpl in *.
  - discriminate Hret.
  - destruct a as [hd t]. destruct (string_dec x hd).
    + subst; invert Hret; auto.
    + right. apply bind_eq_return in Hret as [[ty' l''] [Hret1 Hret2]].
      eapply IHl with l''.
      rewrite Hret1. invert Hret2. reflexivity.
Qed.

Lemma type_and_remove_return_perm: forall x l ty l',
    type_and_remove x l = Return _ (ty, l') ->
    Permutation l ((x, ty)::l').
Proof.
  induction l; intros ty l' Hret; simpl in *.
  - discriminate Hret.
  - destruct a as [hd t]. destruct (string_dec x hd).
    + subst. invert Hret. auto.
    + apply bind_eq_return in Hret as [[ty' l''] [Hret1 Hret2]].
      invert Hret2.
      eapply perm_trans. 2: apply perm_swap.
      constructor. apply IHl; auto.
Qed.

(** Determine the type of a value *)
Fixpoint type_value (v:value) : M ty :=
  match v with
  | val_rval (rval_record lv) =>
    let%err lt := (fix type_rval lv :=
                     match lv with
                     | nil => Return _ nil
                     | (l, v)::tl =>
                       let%err t := type_value v in
                       let%err tl' := type_rval tl in
                       Return _ ((l, t)::tl')
                     end) lv in
    Return _ (ty_rty (rty_record lt))
  | val_num (numval_nat _) => Return _ ty_nat
  | val_num (numval_int _) => Return _ ty_int
  | val_num (numval_timestamp _) => Return _ ty_timestamp
  | val_num (numval_mutez _) => Return _ ty_mutez
  | val_string _ => Return _ ty_string
  | val_bytes _ => Return _ ty_bytes
  | val_constr s v vty =>
    let 'vty_variant vt := vty in
    let%err t := type_value v in
    let%err (t', _) := type_and_remove s vt in
    if (type_eq t t') then Return _ (ty_vty vty)
    else Failed _ (Typing ("Incorrect type passed to constructor "++s))
  | val_bool _ => Return _ ty_bool
  | val_lval (lval_list lv ty) =>
    (fix type_lval lv :=
        match lv with
        | nil => Return _ (ty_list ty)
        | v::tl =>
          let%err t := type_value v in
          if (type_eq t ty)
          then (type_lval tl)
          else Failed _ (Typing ("All the elements in the list should have the annotated type"))
     end) lv
  | val_mval (mval_map lv ty1 ty2) =>
    (fix type_mval lv :=
       match lv with
       | nil => Return _ (ty_map ty1 ty2)
       | (v1, v2)::tl =>
         let%err t1 := type_value v1 in
         let%err t2 := type_value v2 in
         if (type_eq t1 ty1) && (type_eq t2 ty2)
         then (type_mval tl)
         else Failed _ (Typing ("All the elements in the map should have the annotated type"))
       end) lv
  | val_add (addval_tz _) => Return _ ty_key_hash
  | val_add (addval_kt _) => Return _ ty_address
  | val_key _ => Return _ ty_key
  | val_signature _ => Return _ ty_signature
  end.

Theorem type_value_correct : forall G v ty,
    type_value v = Return _ ty ->
    typing_ty_well_formed G ty ->
    typing_val G v ty.
Proof.
  (* induction v using val_ind; intros ty' Heq Hwf; simpl; invert Heq; try constructor. *)
  (* - (* val_num *) *)
  (*   destruct v; invert H0; constructor. *)
  (* - (* record *) *)
  (*   apply bind_eq_return in H1 as [lt [Hlt1 Hlt2]]; invert Hlt2. *)
  (*   generalize dependent lt. *)
  (*   induction lv; intros lt Hwf Hlt; simpl. *)
  (*   + invert Hlt. apply typing_record_nil. *)
  (*   + destruct a as [l0 v]. *)
  (*     apply bind_eq_return in Hlt as [lt' [Hlt1 Hlt2]]. *)
  (*     apply bind_eq_return in Hlt2 as [lt'' [Hlt2 Hlt3]]; invert Hlt3. *)
  (*     apply typing_record_cons; auto. *)
  (*     * invert H; invert Hwf. simpl in *; auto. *)
  (*     * invert H; invert Hwf. specialize (IHlv H3). rewrite Hlt2 in IHlv. simpl in IHlv. apply IHlv; auto. *)
  (*       invert H0. constructor; auto. *)
  (*       intros ty__ HIn. apply H1. apply in_cons; auto. *)
  (* - (* variant *) *)
  (*   destruct vt as [vt]. *)
  (*   apply bind_eq_return in H0 as [t' [Hty Hret]]. *)
  (*   apply bind_eq_return in Hret as [[t'' _lt] [Hrem Hret']]. *)
  (*   destruct type_eq eqn:Heq; try invert Hret'. *)
  (*   apply type_and_remove_return_in in Hrem. *)
  (*   rewrite <- (type_eq_equiv G) in Heq. *)
  (*   invert Hwf. apply IHv in Hty. *)
  (*   + rewrite type_eq_val in Hty; eauto. *)
  (*     apply Join_In_inv in Hrem as [ls2 Hls2]; auto. *)
  (*     apply typing_Tval_constructor with t'' (vty_variant ls2); eauto. *)
  (*   + rewrite wf_eq_type; eauto. apply H1. *)
  (*     clear H0 H1. induction vt; try destruct a; invert Hrem. *)
  (*     * invert H. constructor; auto. *)
  (*     * apply in_cons. auto. *)
  (* - (* list *) *)
  (*   induction lv. *)
  (*   + invert H1. constructor; intuition. *)
  (*   + apply bind_eq_return in H1 as [ty'' [Hty'' Hret]]. *)
  (*     invert H. destruct (type_eq ty'' ty) eqn:Heq; try discriminate Hret. *)
  (*     specialize (IHlv H3 Hret). *)
  (*     invert IHlv. constructor. *)
  (*     intros val__ HIn. destruct HIn; subst; auto. *)
  (*     rewrite <- (type_eq_equiv G) in Heq. *)
  (*     rewrite (type_eq_val G val__ ty ty''); auto. *)
  (*     * apply H2; auto. invert Hwf. rewrite (wf_eq_type _ _ ty); auto. *)
  (*     * apply typing_Teq_sym; auto. *)
  (*     * admit. *)
  (*     * admit. *)
  (* - (* map *) *)
  (*   admit. *)
  (* - (* val_add *) *)
  (*   destruct a; invert H0; constructor. *)
Admitted.

(** Determine the type of an arg *)
Fixpoint type_arg (a:arg) (rt:rty) : M (ty * rty) :=
  match a with
  | arg_var var => let 'rty_record l := rt in
                  let%err (t', l) := type_and_remove var l in
                  Return _ (t', rty_record l)
  | arg_value val => let%err t := type_value val in
                    Return _ (t, rt)
  | arg_record lx =>
    let 'rty_record lt := rt in
    let%err (t, lt') := (fix field_types lx lt :=
                           match lx with
                           | nil => Return _ (nil, lt)
                           | (l, x)::tl => let%err (t, lt') := type_and_remove x lt in
                                         let%err (tl', lt'') := field_types tl lt' in
                                         Return _ ((l, t)::tl', lt'')
                           end) lx lt in
    Return _ (ty_rty (rty_record t), rty_record lt')
  end.

Fixpoint find_fun_types_in_env (fv:fvar) (env:g) : M (ty * ty) :=
  match env with
  | g_empty => Failed _ (Not_found fv)
  | g_fundecl env' fv' ty1 ty2 =>
    if (eqb_string fv fv') then Return _ (ty1, ty2) else find_fun_types_in_env fv env'
  | g_typedef env' _ _ => find_fun_types_in_env fv env'
  end.

(** Determine the type of an applied function *)
Fixpoint type_funct (funct:f) (t:ty) (env:g) : M ty :=
  match funct with
  | fun_dup => Return _ (ty_prod t t)
  | fun_constr s vty =>
    let 'vty_variant vt := vty in
    let%err (t', _) := type_and_remove s vt in
    if (type_eq t t') then Return _ (ty_vty vty)
    else Failed _ (Typing ("Incorrect type passed to constructor "++s))
  | fun_var fv =>
    let%err (ty1, ty2) := find_fun_types_in_env fv env in
    if (type_eq t ty1) then Return _ ty2
    else Failed _ (Typing ("Function "++fv++" expected type "++(string_of_type ty1)++" , " ++(string_of_type t)++" passed instead"))
  | fun_contract t' => match t with
                      | ty_address => Return _ (ty_contract t')
                      | _ => Failed _ (Typing ("Incorrect type "++(string_of_type t)++" passed to function contract"))
                      end
  | fun_address =>  match t with
                   | ty_contract _ => Return _ (ty_address)
                   | _ => Failed _ (Typing ("Incorrect type "++(string_of_type t)++" passed to function address"))
                   end
  | fun_implicit_account => match t with
                           | ty_key_hash => Return _ (ty_contract ty_unit)
                           | _ => Failed _ (Typing ("Incorrect type "++(string_of_type t)++" passed to function implicit_account"))
                           end
  | fun_hash_key => match t with
                   | ty_key => Return _ (ty_key)
                   | _ => Failed _ (Typing ("Incorrect type "++(string_of_type t)++" passed to function hash_key"))
                   end
  | fun_blake2b | fun_sha256 | fun_sha512 =>
                               match t with
                               | ty_key => Return _ (ty_key)
                               | _ => Failed _ (Typing ("Incorrect type "++(string_of_type t)++" passed to cryptographic primitive"))
                               end
  end.

Lemma type_funct_eq_pre : forall G f t t' t'',
    typing_type_eq G t t' ->
    (typing_f G f t t'' <-> typing_f G f t' t'').
Proof.
  (* TODO *)
Admitted.

Lemma type_funct_correct : forall G funct ty ty',
    type_funct funct ty G = Return _ ty' ->
    typing_ty_well_formed G ty' ->
    typing_f G funct ty ty'.
Proof.
  (* induction funct; intros ty ty' Hty Hwf; simpl in Hty; invert Hty. *)
  (* - (* fun_var *) admit. *)
  (* - (* dup *) constructor. *)
  (* - (* constr *) *)
  (*   destruct vty5 as [vt]. *)
  (*   apply bind_eq_return in H0 as [[t' _vt] [Hrem Hret]]. *)
  (*   destruct type_eq eqn: Heq; invert Hret. *)
  (*   rewrite <- (type_eq_equiv G) in Heq. *)
  (*   apply type_and_remove_return_in in Hrem. *)
  (*   apply Join_In_inv in Hrem as [ls2 Hls2]. *)
  (*   rewrite type_funct_eq_pre; eauto. *)
  (*   apply typing_Tf_constructor with (vty_variant ls2); auto. *)
  (*   invert Hwf; auto. *)
Admitted.

Lemma type_funct_complete : forall G funct ty ty',
    typing_f G funct ty ty' ->
    typing_ty_well_formed G ty' ->
    type_funct funct ty G = Return _ ty'.
Proof.
  (* induction funct; intros ty ty' Hty Hwf; simpl. *)
  (* - (* fun_var *) admit. *)
  (* - (* dup *) invert Hty; reflexivity. *)
  (* - (* constr *) *)
  (*   invert Hty. destruct vty5 as [lt1]; destruct vty' as [lt2]. *)
  (*   apply (Join_In_l (constructor5, ty)) in H4; try constructor; auto. *)
  (*   invert Hwf. apply type_and_remove_in_return in H4 as [lt' Hlt']; auto. *)
  (*   rewrite Hlt'; simpl. *)
  (*   assert (type_eq ty ty = true) as Htyty. *)
  (*   { rewrite <- (type_eq_equiv G). constructor. } *)
  (*   rewrite Htyty. reflexivity. *)
Admitted.

Theorem type_funct_equiv : forall G funct ty ty',
    typing_ty_well_formed G ty' ->
    (typing_f G funct ty ty' <->
     type_funct funct ty G = Return _ ty').
Proof.
  split; intros.
  + apply type_funct_complete; auto.
  + apply type_funct_correct; auto.
Qed.

Open Scope string_scope.

Definition fail_diverging_match {A} t rt t' rt' :=
  Failed A (Typing ("Match fails: Two branches of a rhs match should have the same type:\n"
                      ++ (string_of_type t)++"->"++(string_of_type (ty_rty rt))
                      ++ "\nis not compatible with \n"
                      ++  (string_of_type t')++"->"++(string_of_type (ty_rty rt')))).

Definition fail_wrong_constructor {A} t constr :=
  Failed A (Typing ("Match fails: constructor "++constr++" does not belong to type "++(string_of_type t)++".")).

Definition fail_wrong_constructor_or {A} s1 s2 :=
  Failed A (Typing ("The constructors for type 'or' are 'Left' and 'Right' not '"++s1++"' and '"++s2++"';")).

Definition fail_wrong_constructor_option {A} s1 s2 :=
  Failed A (Typing ("The constructors for type 'option' are 'None' and 'Some' not '"++s1++"' and '"++s2++"';")).

Definition fail_wrong_constructor_bool {A} s1 s2 :=
  Failed A (Typing ("The constructors for type 'bool' are 'True' and 'False' not '"++s1++"' and '"++s2++"';")).

Definition fail_missing_cases {A} t (constrs : list (constructor*ty)) :=
  Failed A (Typing ("Match fails: non-exhaustive match, missing case(s) for type "
                      ++(string_of_type t)++" are "
                      ++(String.concat ", " (List.map (fun '(l, t) => l) constrs))
                      ++".")).
Definition fail_missing {A} t (constrs : list (constructor*ty)) :=
  Failed A (Typing ("Match fails: non-exhaustive match, some case(s) are missing for type "
                      ++(string_of_type t)++".")).

Definition fail_wrong_pattern {A} t pat :=
  Failed A (Typing ("Match fails: Pattern "++(string_of_pattern pat)++" doesn't belong to type "++ (string_of_type t)++"or is already covered by a previous pattern")).

Definition fail_wrong_patterns {A} {B} t (lbr : list (pattern*B)) :=
  Failed A (Typing ("Match fails: Type "
                      ++(string_of_type t)++" cannot be matched against "++
                      match List.map (fun '(pat,_) => string_of_pattern pat) lbr with
                        nil => "empty pattern list"
                      | patterns => "the folowing patterns:\n|"
                                     ++ (String.concat "\n| " patterns)
                                     ++"."
                      end)).


(** Determine the type of a right-hand side *)
Fixpoint type_rhs (r:rhs) (rt:rty) (env:g) {struct r} : M (ty * rty) :=
  let 'rty_record l := rt in
  match r with
  | rhs_arg a => type_arg a rt
  | rhs_app f a => let%err (t, rt') := type_arg a rt in
                  let%err t' := type_funct f t env in
                  Return _ (t', rt')
  | rhs_projection var lab =>
    let%err (t, rt') := type_and_remove var l in
    match t with
    | ty_rty (rty_record l2) => let%err (ty, _) := type_and_remove lab l2 in
                               Return _ (ty, rty_record rt')
    | _ => Failed _ (Typing ("Projection expects "++var++" to be a record"))
    end
  | rhs_update var lx =>
    let slx := (LexicoLabSort.sort lx) in
    let%err (ty, stack') := type_and_remove var l in
    match ty with
    | ty_rty (rty_record slt) =>
      (fix type_update lx lt st {struct lt} :=
         match lx, lt with
         | nil,_ => Return _ (ty, rty_record st)
         | _, nil => Failed _ (Typing ("Record update expects the updated record to be longer than the update"))
         | (l, x)::tl, (l', t)::tl' =>
           if (eqb_string l l')
           then let%err (t', st') := type_and_remove x st in
                if (type_eq t t')
                then (type_update tl tl' st')
                else Failed _ (Typing ("Type "++(string_of_type t)++" was expected for "++l++" in "++var++" update, "++(string_of_type t')++" was found instead"))
           else type_update ((l, x)::tl) tl' st
         end) slx slt stack'
    | _ => Failed _ (Typing ("Record update expects "++var++" to be a record"))
    end
  | rhs_add x1 x2 =>
    let%err (t1, l') := type_and_remove x1 l in
    let%err (t2, l'') := type_and_remove x2 l' in
    match t1, t2 with
    | ty_nat, ty_nat => Return _ (ty_nat, rty_record l'')
    | ty_nat, ty_int | ty_int, ty_nat | ty_int, ty_int => Return _ (ty_int, rty_record l'')
    | ty_timestamp, ty_int | ty_int, ty_timestamp => Return _ (ty_timestamp, rty_record l'')
    | ty_mutez, ty_mutez => Return _ (ty_mutez, rty_record l'')
    | _, _ => Failed _ (Typing ("Wrong input types for binary operator add"))
    end
  | rhs_sub x1 x2 =>
    let%err (t1, l') := type_and_remove x1 l in
    let%err (t2, l'') := type_and_remove x2 l' in
    match t1, t2 with
    | ty_nat, ty_nat | ty_nat, ty_int
    | ty_int, ty_nat | ty_int, ty_int => Return _ (ty_int, rty_record l'')
    | ty_timestamp, ty_int | ty_timestamp, ty_timestamp => Return _ (ty_int, rty_record l'')
    | ty_mutez, ty_mutez => Return _ (ty_mutez, rty_record l'')
    | _, _ => Failed _ (Typing ("Wrong input types for binary operator sub"))
    end
  | rhs_mul x1 x2 =>
    let%err (t1, l') := type_and_remove x1 l in
    let%err (t2, l'') := type_and_remove x2 l' in
    match t1, t2 with
    | ty_nat, ty_nat => Return _ (ty_nat, rty_record l'')
    | ty_nat, ty_int | ty_int, ty_nat | ty_int, ty_int => Return _ (ty_int, rty_record l'')
    | ty_nat, ty_mutez | ty_mutez, ty_nat => Return _ (ty_mutez, rty_record l'')
    | _, _ => Failed _ (Typing ("Wrong input types for binary operator mul"))
    end
  | rhs_ediv x1 x2 =>
    let%err (t1, l') := type_and_remove x1 l in
    let%err (t2, l'') := type_and_remove x2 l' in
    match t1, t2 with
    | ty_nat, ty_nat =>
      Return _ (ty_rty (rty_record (("quotient", ty_nat)::("remainder", ty_nat)::nil)), rty_record l'')
    | ty_nat, ty_int
    | ty_int, ty_nat
    | ty_int, ty_int =>
      Return _ (ty_rty (rty_record (("quotient", ty_int)::("remainder", ty_nat)::nil)), rty_record l'')
    | ty_mutez, ty_nat =>
      Return _ (ty_rty (rty_record (("quotient", ty_mutez)::("remainder", ty_mutez)::nil)), rty_record l'')
    | ty_mutez, ty_mutez =>
      Return _ (ty_rty (rty_record (("quotient", ty_nat)::("remainder", ty_mutez)::nil)), rty_record l'')
    | _, _ => Failed _ (Typing ("Wrong input types for binary operator ediv"))
    end
  | rhs_not x =>
    let%err (t, l') := type_and_remove x l in
    match t with
    | ty_nat | ty_int => Return _ (ty_int, rty_record l')
    | t => if (is_bool t) then Return _ (ty_bool, rty_record l')
          else Failed _ (Typing ("Wrong input type for unnary operator not"))
    end
  | rhs_and x1 x2 =>
    let%err (t1, l') := type_and_remove x1 l in
    let%err (t2, l'') := type_and_remove x2 l' in
    match t1, t2 with
    | ty_nat, ty_nat | ty_int, ty_nat => Return _ (ty_nat, rty_record l'')
    | t1, t2 => if (is_bool t1) && (is_bool t2)
               then Return _ (ty_bool, rty_record l'')
               else Failed _ (Typing ("Wrong input types for binary operator and"))
    end
  | rhs_or x1 x2 | rhs_xor x1 x2 =>
                   let%err (t1, l') := type_and_remove x1 l in
                   let%err (t2, l'') := type_and_remove x2 l' in
                   match t1, t2 with
                   | ty_nat, ty_nat => Return _ (ty_nat, rty_record l'')
                   | t1, t2 => if (is_bool t1) && (is_bool t2)
                              then Return _ (ty_bool, rty_record l'')
                              else Failed _ (Typing ("Wrong input types for binary operator or/xor"))
                   end
  | rhs_lsl x1 x2 | rhs_lsr x1 x2 =>
                    let%err (t1, l') := type_and_remove x1 l in
                    let%err (t2, l'') := type_and_remove x2 l' in
                    match t1, t2 with
                    | ty_nat, ty_nat => Return _ (ty_nat, rty_record l'')
                    | _, _ => Failed _ (Typing ("Wrong input types for binary operator lsl/lsr"))
                    end
  | rhs_abs x =>
    let%err (t, l') := type_and_remove x l in
    match t with
    | ty_int => Return _ (ty_nat, rty_record l')
    | _ => Failed _ (Typing ("Wrong input type for abs"))
    end
  | rhs_is_nat x =>
    let%err (t, l') := type_and_remove x l in
    match t with
    | ty_int => Return _ (ty_option ty_nat, rty_record l')
    | _ => Failed _ (Typing ("Wrong input type for is_nat"))
    end
  | rhs_int x =>
    let%err (t, l') := type_and_remove x l in
    match t with
    | ty_nat => Return _ (ty_int, rty_record l')
    | _ => Failed _ (Typing ("Wrong input type for int"))
    end
  | rhs_match var branches =>
    let 'branchesrhs_branches lbr := branches in
    let%err (tvar, lt') := type_and_remove var l in

    match tvar, lbr with
    | (ty_vty (vty_variant ((s1, t1)::vt))), (pattern_variant s2 l2 , rhs2)::lbr =>
      if (eqb_string s1 s2) then
        let%err (t, rt'') := type_rhs rhs2 (rty_record ((l2, t1)::lt')) env in
        (fix type_branches vt lbr {struct lbr} :=
           match vt, lbr with
           | nil, nil => Return _ (t, rt'')
           | nil, (pat,_)::_ => fail_wrong_pattern tvar pat
           | _::_, nil => fail_missing_cases tvar vt
           | (s1, t1)::tl1, (pattern_any,rhs2)::tl2 =>
             let%err (t', rt''') := type_rhs rhs2 (rty_record lt') env in
             if (andb (type_eq t t') (rty_eq rt'' rt'''))
             then Return _ (t,rt'')
             else fail_diverging_match  t rt'' t' rt'''
           | (s1, t1)::tl1, (pattern_variant s2 l2, rhs2)::tl2 =>
             if (eqb_string s1 s2)
             then let%err (t', rt''') := type_rhs rhs2 (rty_record ((l2, t1)::lt')) env in
                  if (andb (type_eq t t') (rty_eq rt'' rt'''))
                  then (type_branches tl1 tl2)
                  else fail_diverging_match  t rt'' t' rt'''
             else fail_wrong_constructor tvar s2
           | (s1, t1)::tl1, (pat,rhs2)::tl2 =>
             fail_wrong_pattern tvar pat
           end) vt lbr
      else fail_wrong_constructor tvar s2
    | ty_or ty1 ty2, (pattern_variant s1 l1, rhs1)::(pattern_variant s2 l2, rhs2)::nil =>
      if (eqb_string s1 "Left") && (eqb_string s2 "Right") then
        let%err (t1, rt1) := type_rhs rhs1 (rty_record ((l1, ty1)::lt')) env in
        let%err (t2, rt2) := type_rhs rhs2 (rty_record ((l2, ty2)::lt')) env in
        if (type_eq t1 t2) && (rty_eq rt1 rt2)
        then Return _ (t1, rt1)
        else fail_diverging_match t1 rt1 t2 rt2
      else fail_wrong_constructor_or s1 s2
    | ty_option ty, (pattern_variant s1 l1, rhs1)::(pattern_variant s2 l2, rhs2)::nil =>
      if (eqb_string s1 "None") && (eqb_string s2 "Some") then
        let%err (t1, rt1) := type_rhs rhs1 (rty_record ((l1, ty_unit)::lt')) env in
        let%err (t2, rt2) := type_rhs rhs2 (rty_record ((l2, ty)::lt')) env in
        if (type_eq t1 t2) && (rty_eq rt1 rt2)
        then Return _ (t1, rt1)
        else fail_diverging_match t1 rt1 t2 rt2
      else fail_wrong_constructor_option s1 s2
    | ty_bool, (pattern_variant s1 l1, rhs1)::(pattern_variant s2 l2, rhs2)::nil =>
      if (eqb_string s1 "False") && (eqb_string s2 "True") then
        let%err (t1, rt1) := type_rhs rhs1 (rty_record ((l1, ty_unit)::lt')) env in
        let%err (t2, rt2) := type_rhs rhs2 (rty_record ((l2, ty_unit)::lt')) env in
        if (type_eq t1 t2) && (rty_eq rt1 rt2)
        then Return _ (t1, rt1)
        else fail_diverging_match t1 rt1 t2 rt2
      else fail_wrong_constructor_bool s1 s2
    | ty_list ty, (pattern_nil,rhs_nil)::(pattern_cons lhd ltl,rhs_cons)::nil =>
      let%err (tnil, rtnil) := type_rhs rhs_nil (rty_record lt') env in
      let%err (tcons, rtcons) :=
         type_rhs rhs_cons (rty_record ((lhd, ty)::(ltl, ty_list ty)::lt')) env in
      if (type_eq tnil tcons) && (rty_eq rtnil rtcons)
      then Return _ (tnil, rtnil)
      else fail_diverging_match tnil rtnil tcons rtcons
    | ty_list ty, (pattern_nil,rhs_nil)::(pattern_any,rhs_any)::nil =>
      let%err (tnil, rtnil) := type_rhs rhs_nil (rty_record lt') env in
      let%err (tany, rtany) := type_rhs rhs_any (rty_record lt') env in
      if (type_eq tnil tany) && (rty_eq rtnil rtany)
      then Return _ (tnil, rtnil)
      else fail_diverging_match tnil rtnil tany rtany
    | ty_list ty, (pattern_cons lhd ltl,rhs_cons)::(pattern_any,rhs_any)::nil =>
      let%err (tany, rtany) := type_rhs rhs_any (rty_record lt') env in
      let%err (tcons, rtcons) :=
         type_rhs rhs_cons (rty_record ((lhd, ty)::(ltl, ty_list ty)::lt')) env in
      if (type_eq tcons tany) && (rty_eq rtcons rtany)
      then Return _ (tcons, rtcons)
      else fail_diverging_match tcons rtcons tany rtany
    | _, _ => fail_wrong_patterns tvar lbr
    end
  | rhs_concat x1 x2 =>
    let%err (t1, l') := type_and_remove x1 l in
    let%err (t2, l'') := type_and_remove x2 l' in
    match t1, t2 with
    | ty_string, ty_string => Return _ (ty_string, rty_record l'')
    | ty_bytes, ty_bytes => Return _ (ty_bytes, rty_record l'')
    | _, _ => Failed _ (Typing "Wrong input types for concat")
    end
  | rhs_size x =>
    let%err (t, l') := type_and_remove x l in
    match t with
    | ty_string => Return _ (ty_nat, rty_record l')
    | ty_bytes => Return _ (ty_nat, rty_record l')
    | _ => Failed _ (Typing "Wrong input type for size")
    end
  | rhs_slice x1 x2 x3 =>
    let%err (t1, l') := type_and_remove x1 l in
    let%err (t2, l'') := type_and_remove x2 l' in
    let%err (t3, l''') := type_and_remove x3 l'' in
    match t1, t2, t3 with
    | ty_string, ty_nat, ty_nat => Return _ (ty_option ty_string, rty_record l''')
    | ty_bytes, ty_nat, ty_nat => Return _ (ty_option ty_string, rty_record l''')
    | _,_,_ => Failed _ (Typing "Wrong input type for slice")
    end
  | rhs_bytes_pack x =>
    let%err (t, l') := type_and_remove x l in
    Return _ (ty_bytes, rty_record l')
  | rhs_bytes_unpack ty x =>
    let%err (t, l') := type_and_remove x l in
    match t with
    | ty_bytes => Return _ (ty, rty_record l')
    | _ => Failed _ (Typing "Wrong input type for unpack")
    end
  | rhs_eq x1 x2 | rhs_neq x1 x2 =>
                   let%err (t1, l') := type_and_remove x1 l in
                   let%err (t2, l'') := type_and_remove x2 l' in
                   match t1, t2 with
                   | ty_nat, ty_nat | ty_int, ty_int
                   | ty_mutez, ty_mutez | ty_timestamp, ty_timestamp
                   | ty_string, ty_string | ty_bytes, ty_bytes
                   | ty_address, ty_address =>
                     Return _ (ty_bool, rty_record l'')
                   | _, _ => Failed _ (Typing "Wrong input types for comparison")
                   end
  | rhs_inf x1 x2 | rhs_infeq x1 x2
  | rhs_sup x1 x2 | rhs_supeq x1 x2 =>
                    let%err (t1, l') := type_and_remove x1 l in
                    let%err (t2, l'') := type_and_remove x2 l' in
                    match t1, t2 with
                    | ty_nat, ty_nat | ty_int, ty_int
                    | ty_mutez, ty_mutez | ty_timestamp, ty_timestamp
                    | ty_string, ty_string | ty_bytes, ty_bytes =>
                                             Return _ (ty_bool, rty_record l'')
                    | _, _ => Failed _ (Typing "Wrong input types for comparison")
                    end
  | rhs_list ll =>
    match ll with
    | nil => Failed _ (Typing "List constructed from variables should be non empty")
    | hd::tl => let%err (ty, l') := type_and_remove hd l in
              (fix type_check_list ll l :=
                 match ll with
                 | nil => Return _ (ty_list ty, rty_record l)
                 | hd::tl => let%err (ty', l') := type_and_remove hd l in
                           if (type_eq ty ty') then (type_check_list tl l')
                           else Failed _ (Typing "All the variables used to construct a list should have the same type")
                 end) tl l'
    end
  | rhs_list_cons l1 l2 =>
    let%err (ty1, l') := type_and_remove l1 l in
    let%err (ty2, l'') := type_and_remove l2 l' in
    if (type_eq (ty_list ty1) ty2) then Return _ ((ty_list ty1), rty_record l'')
    else Failed _ (Typing "Right argument of cons should be a list of elements of the left argument")
  | rhs_list_map l1 l2 r =>
    let%err (ty1, l') := type_and_remove l1 l in
    match ty1 with
    | ty_list ty1' => let%err (ty, rty) := type_rhs r (rty_record ((l2, ty1')::l')) env in
                     if rty_eq (rty_record l') rty then Return _ (ty_list ty, rty)
                     else Failed _ (Typing "list_map should consume all it's args")
    | _ => Failed _ (Typing "Argument of list_map should be a list")
    end
  | rhs_map ll =>
    match ll with
    | nil => Failed _ (Typing "Map constructed from variables should be non empty")
    | (l1, l2)::tl => let%err (ty1, l') := type_and_remove l1 l in
                    let%err (ty2, l'') := type_and_remove l2 l' in
                    (fix type_check_list ll l :=
                       match ll with
                       | nil => Return _ (ty_map ty1 ty2, rty_record l)
                       | (l1, l2)::tl => let%err (ty1', l') := type_and_remove l1 l in
                                       let%err (ty2', l'') := type_and_remove l2 l' in
                                       if (type_eq ty1 ty1') && (type_eq ty2 ty2') then
                                         type_check_list tl l''
                                       else Failed _ (Typing "All the variables used to construct a list should have the same type")
                       end) tl l''
    end
  | rhs_map_map l1 l2 var r =>
    let%err (ty, l') := type_and_remove var l in
    match ty with
    | ty_map ty1 ty2 => let%err (ty2', rty') :=
                          type_rhs r (rty_record ((l1, ty1)::(l2, ty2)::l')) env in
                       if rty_eq (rty_record l') rty' then
                         Return _ (ty_map ty1 ty2', (rty_record l'))
                       else Failed _ (Typing "map_map should consume all it's arguments")
    | _ => Failed _ (Typing "Argument of map_map should be a map")
    end
  | rhs_map_get var lab =>
    let%err (ty, l') := type_and_remove var l in
    match ty with
    | ty_map ty1 ty2 => let%err (ty1', l'') :=
                          type_and_remove lab l' in
                       if type_eq ty1 ty1' then
                         Return _ (ty_option ty2, rty_record l'')
                       else Failed _ (Typing ("map_get on map of type "++(string_of_type ty)++" expects a key of type "++(string_of_type ty1)++", "++(string_of_type ty1')++" was used instead"))
    | _ => Failed _ (Typing "map_get should be called on a map")
    end
  | rhs_map_update var l1 l2 =>
    let%err (ty, l') := type_and_remove var l in
    match ty with
    | ty_map ty1 ty2 =>
      let%err (ty1', l'') := type_and_remove l1 l' in
      let%err (ty2', l''') := type_and_remove l2 l'' in
      if (type_eq ty1 ty1') && (type_eq (ty_option ty2) ty2') then
        Return _ (ty, rty_record l''')
      else Failed _ (Typing ("map_update on map of type "++(string_of_type ty)++" was called with a key of type "++(string_of_type ty1')++" and a value of type "++(string_of_type ty2')))
    | _ => Failed _ (Typing "map_update should be called on a map")
    end
  | rhs_check_signature var_k var_sig var_by =>
    let%err (tyk, l') := type_and_remove var_k l in
    let%err (tysig, l'') := type_and_remove var_sig l' in
    let%err (tyby, l''') := type_and_remove var_by l'' in
    match tyk, tysig, tyby with
    | ty_key, ty_sig, ty_bytes => Return _ (ty_bool, rty_record l''')
    | _,_,_ => Failed _ (Typing "Wrong types passed to check_signature, should be key, signature and bytes")
    end
  | rhs_amount => Return _ (ty_mutez, rty_record l)
  | rhs_self => Return _ (ty_contract ty_unit, rty_record l) (* TODO get the type of self ? *)
  | rhs_transfer_tokens var_param var_am var_dest =>
    let%err (typ, l') := type_and_remove var_param l in
    let%err (tyam, l'') := type_and_remove var_am l' in
    let%err (tydest, l''') := type_and_remove var_dest l'' in
    if (type_eq (ty_contract typ) tydest) && (type_eq tyam ty_mutez)
    then Return _ (ty_operation, rty_record l''')
    else Failed _ (Typing "Wrong types passed to transfer_tokens")
  | rhs_set_delegate var =>
    let%err (ty, l') := type_and_remove var l in
    if (type_eq ty (ty_option ty_key_hash)) then Return _ (ty_operation, rty_record l')
    else Failed _ (Typing ("Wrong type passed to set_delegate : (option key_hash expected), "++(string_of_type ty)++" was found instead"))
  | rhs_sender | rhs_source => Return _ (ty_address, rty_record l)
  | rhs_now => Return _ (ty_timestamp, rty_record l)
  end.

(** Determine the type of a left-hand side *)
Fixpoint type_lhs (l:lhs) (t:ty) (rt:rty) : M rty :=
  match l with
  | lhs_var var => let 'rty_record l := rt in Return _ (rty_record (LexicoTySort.sort ((var, t)::l)))
  | lhs_record lx =>
    let 'rty_record l := rt in
    match t with
    | ty_rty (rty_record lt) =>
      let%err l := (fix field_types lx :=
                      match lx with
                      | nil => Return _ l
                      | (lab, x)::tl => let%err (t, _) := type_and_remove lab lt in
                                      let%err tl' := field_types tl in
                                      Return _ ((x, t)::tl')
                      end) lx in
      Return _ (rty_record (LexicoTySort.sort l))
    | ty_prod ty1 ty2 =>
      match lx with
      | (l1, x1)::(l2, x2)::nil =>
        if (eqb_string l1 "car") && (eqb_string l2 "cdr")
        then Return _ (rty_record (LexicoTySort.sort ((x1, ty1)::(x2, ty2)::l)))
        else Failed _ (Typing "The two labels to destruct a product should be car and cdr")
      | _ => Failed _ (Typing "A producted should be destructured in with two branches")
      end
    | _ => Failed _ (Typing "Record left-hand side should receive either a record or a product")
    end
  end.

Inductive type_checker_result :=
| Any
| Inferred (rt:rty).

Definition no_tail_fail (res:M type_checker_result): M rty :=
  let%err r := res  in
  match r with
  | Any => Failed _ (Typing "Tail fail")
  | Inferred rt => Return _ rt
  end.

Definition fail_diverging_match_instr {A} rt rt' :=
  Failed A (Typing ("Match instruction fails: Two branches of the match instruction should have the same type:\n"
                      ++ (string_of_type (ty_rty rt))
                      ++ "\nis not compatible with \n"
                      ++ (string_of_type (ty_rty rt')))).



Definition unify_branches rt1 rt2 :=
   match rt1, rt2 with
   | Any, Any => Return _ Any
   | Any, _ => Return _ rt2
   | _, Any => Return _ rt1
   | Inferred rt1', Inferred rt2' =>
     if (rty_eq rt1' rt2') then Return _ rt1
     else fail_diverging_match_instr rt1' rt2'
   end.

(** Determine the type of an instruction *)
Fixpoint type_instr (ins:instruction) (rt:rty) (env:g) {struct ins} : M type_checker_result :=
  match ins with
  | instr_noop => Return _ (Inferred rt)
  | instr_seq ins1 ins2 => let%err t' := no_tail_fail (type_instr ins1 rt env) in
                          type_instr ins2 t' env
  | instr_assign lhs rhs => let%err (t, rt) := type_rhs rhs rt env in
                           let%err rt' := type_lhs lhs t rt in
                           Return _ (Inferred rt')
  | instr_drop var =>
    let 'rty_record lt := rt in let%err (_, rt') := type_and_remove var lt in
                                Return _ (Inferred (rty_record rt'))
  | instr_match var branches =>
    let 'branchesins_branches lbr := branches in
    let 'rty_record lt := rt in
    let%err (tvar, lt') := type_and_remove var lt in
    match tvar, lbr with
    | ty_vty (vty_variant vt), lbr =>
         (fix type_branches vt lbr currentRt {struct lbr} :=
            match vt, lbr with
            | nil, nil => Return _ currentRt
            | nil, (pat,_)::_ => fail_wrong_pattern tvar pat
            | _, nil => fail_missing_cases tvar vt
            | (s1, t1)::tl1, (pattern_any,ins2)::tl2 =>
              let%err rt1 := type_instr ins2 (rty_record lt') env in
              unify_branches currentRt rt1
            | (s1, t1)::tl1, (pattern_variant s2 l2, ins2)::tl2 =>
              if (eqb_string s1 s2)
              then let%err rt2 :=
                      type_instr ins2 (rty_record (LexicoTySort.sort ((l2, t1)::lt'))) env in
                   let%err unified_ty := unify_branches currentRt rt2
                   in type_branches tl1 tl2 unified_ty
              else fail_wrong_constructor tvar s2
            | _, (pat,_)::_ => fail_wrong_pattern tvar pat
            end) vt lbr Any
    | ty_or ty1 ty2, (pattern_variant s1 l1, ins1)::(pattern_variant s2 l2, ins2)::nil =>
      if (eqb_string s1 "Left") && (eqb_string s2 "Right") then
        let%err rt1 := type_instr ins1 (rty_record (LexicoTySort.sort ((l1, ty1)::lt'))) env in
        let%err rt2 := type_instr ins2 (rty_record (LexicoTySort.sort ((l2, ty2)::lt'))) env in
        unify_branches rt1 rt2
      else fail_wrong_constructor_or s1 s2
    | ty_option ty, (pattern_variant s1 l1, ins1)::(pattern_variant s2 l2, ins2)::nil =>
      if (eqb_string s1 "None") && (eqb_string s2 "Some") then
        let%err rt1 := type_instr ins1 (rty_record (LexicoTySort.sort ((l1, ty_unit)::lt'))) env in
        let%err rt2 := type_instr ins2 (rty_record (LexicoTySort.sort ((l2, ty)::lt'))) env in
        unify_branches rt1 rt2
      else fail_wrong_constructor_option s1 s2
    | ty_bool, (pattern_variant s1 l1, ins1)::(pattern_variant s2 l2, ins2)::nil =>
      if (eqb_string s1 "False") && (eqb_string s2 "True") then
        let%err rt1 := type_instr ins1 (rty_record (LexicoTySort.sort ((l1, ty_unit)::lt'))) env in
        let%err rt2 := type_instr ins2 (rty_record (LexicoTySort.sort ((l2, ty_unit)::lt'))) env in
        unify_branches rt1 rt2
      else fail_wrong_constructor_bool s1 s2
    | ty_list ty, (pattern_nil,ins_nil)::(pattern_cons lhd ltl,ins_cons)::nil =>
      let%err rtnil := type_instr ins_nil (rty_record lt') env in
      let%err rtcons := type_instr ins_cons (rty_record (LexicoTySort.sort ((lhd, ty)::(ltl, ty_list ty)::lt'))) env in
      (unify_branches rtnil rtcons)
    | ty_list ty, (pattern_nil,ins_nil)::(pattern_any,ins_any)::nil =>
      let%err rtnil := type_instr ins_nil (rty_record lt') env in
      let%err rtany := type_instr ins_any (rty_record lt') env in
      unify_branches rtnil rtany
    | ty_list ty, (pattern_cons lhd ltl,ins_cons)::(pattern_any,ins_any)::nil =>
      let%err rtany := type_instr ins_any (rty_record lt') env in
      let%err rtcons := type_instr ins_cons (rty_record ((lhd, ty)::(ltl, ty_list ty)::lt')) env in
      unify_branches rtcons rtany
    | _, _ => fail_wrong_patterns tvar lbr
    end
  | instr_loop_left var var' body =>
    let 'rty_record lt := rt in
    let%err (tvar, lt') := type_and_remove var lt in

    match tvar with
    | ty_or ty1 ty2 | ty_vty (vty_variant (("Left", ty1)::("Right", ty2)::nil))
                      => let%err rt'' :=
                           no_tail_fail
                             (type_instr body
                                         (rty_record (LexicoTySort.sort ((var', ty1)::lt')))
                                         env) in
                        if (rty_eq rt rt'')
                        then Return _ (Inferred (rty_record lt'))
                        else Failed _ (Typing "loop_left return rty is incorrect")
    | _ => Failed _ (Typing "loop_left should be applied either to an or type or an equivalent variant")
    end
  | instr_loop var body =>
    let 'rty_record lt := rt in
    let%err (tvar, lt') := type_and_remove var lt in
    match tvar with
    | ty_bool | ty_vty (vty_variant (("False", ty_unit)::("True", ty_unit)::nil))
                => let%err rt'' := no_tail_fail (type_instr body (rty_record lt') env) in
                  if (rty_eq rt rt'')
                  then Return _ (Inferred (rty_record lt'))
                  else Failed _ (Typing "loop return rty is incorrect")
    | _ => Failed _ (Typing "loop should be applied either to bool or an equivalent variant")
    end
  | instr_list_for l2 l1 i =>
    let 'rty_record lt := rt in
    let%err (ty1, l') := type_and_remove l1 lt in
    match ty1 with
    | ty_list ty1' =>
      let%err rt :=
         no_tail_fail (type_instr i (rty_record (LexicoTySort.sort ((l2, ty1')::l'))) env) in
      if (rty_eq rt (rty_record l')) then Return _ (Inferred rt)
      else Failed _ (Typing "list for return rty is incorrect")
    | _ => Failed _ (Typing "Argument of list_for should be a list")
    end
  | instr_map_for l1 l2 var i =>
    let 'rty_record lt := rt in
    let%err (ty, l') := type_and_remove var lt in
    match ty with
    | ty_map ty1 ty2 =>
      let%err rt := no_tail_fail (type_instr i (rty_record (LexicoTySort.sort ((l1, ty1)::(l2, ty2)::l'))) env) in
      Return _ (Inferred rt)
    | _ => Failed _ (Typing "Argument of map_for should be a map")
    end
  | instr_failwith arg => let%err _ := type_arg arg rt in
                         Return _ Any
  end.

(** Typed Ast *)
Inductive typed_instr : Set :=
| ty_instr_noop (ty1:rty)
| ty_instr_seq (ty1:rty) (ins1:typed_instr) (ins2:typed_instr) (ty2:type_checker_result)
| ty_instr_assign (ty1:rty) (l:lhs) (t:ty) (r:rhs) (ty2:rty)
| ty_instr_drop (ty1:rty) (var:label) (ty2:rty)
| ty_instr_match (ty1:rty) (var:label) (branches:(list (pattern * typed_instr))) (ty2:type_checker_result)
| ty_instr_loop_left (ty1:rty) (var:label) (var':label) (body:typed_instr) (ty2:rty)
| ty_instr_loop (ty1:rty) (var:label) (body:typed_instr) (ty2:rty)
| ty_instr_list_for (ty1:rty) (var:label) (var':label) (body:typed_instr) (ty2:rty)
| ty_instr_map_for (ty1:rty) (var1:label) (var2:label) (var:label) (i1:typed_instr) (ty2:rty)
| ty_instr_failwith (ty1:rty) (a:arg).

(** Returns the Inferred types of a typed instruction (which does not tail-fail) *)
Definition types_of_typed_instr (ins:typed_instr) : M (rty * rty) :=
  match ins with
  | ty_instr_noop t => Return _ (t, t)
  | ty_instr_seq t1 _ _ t2 => let%err t2' := no_tail_fail (Return _ t2) in
  Return _ (t1, t2')
  | ty_instr_assign t1 _ _ _ t2 => Return _ (t1, t2)
  | ty_instr_drop t1 _ t2 => Return _ (t1, t2)
  | ty_instr_match t1 _ _ t2 => let%err t2' := no_tail_fail (Return _ t2) in
 Return _ (t1, t2')
  | ty_instr_loop_left t1 _ _ _ t2 => Return _ (t1, t2)
  | ty_instr_loop t1 _ _ t2 => Return _ (t1, t2)
  | ty_instr_list_for t1 _ _ _ t2 => Return _ (t1, t2)
  | ty_instr_map_for t1 _ _ _ _ t2 => Return _ (t1, t2)
  | ty_instr_failwith t1 _ => Failed _ (Typing "Tail fail")
  end.


Definition should_not_happen {A} := Failed A Should_not_happen.

Fixpoint typed_instr_of_instr (ins:instruction) (rt:rty) (env:g) : M typed_instr :=
  match ins with
  | instr_noop => Return _ (ty_instr_noop rt)
  | instr_seq ins1 ins2 =>
    let%err rt' := no_tail_fail (type_instr ins1 rt env) in
    let%err rt'' := type_instr ins2 rt' env in
    let%err ins1' := typed_instr_of_instr ins1 rt env in
    let%err ins2' := typed_instr_of_instr ins2 rt' env in
    Return _ (ty_instr_seq rt ins1' ins2' rt'')
  | instr_assign lhs rhs =>
    let%err (t, _) := type_rhs rhs rt env in
    let%err rt' := no_tail_fail (type_instr (instr_assign lhs rhs) rt env) in
    Return _ (ty_instr_assign rt lhs t rhs rt')
  | instr_drop var =>
    let%err rt' := no_tail_fail (type_instr ins rt env) in
    Return _ (ty_instr_drop rt var rt')
  | instr_match var branches =>
    let    'branchesins_branches lbr := branches in
    let    'rty_record lt := rt in
    let%err rt' := type_instr ins rt env in
    let%err (tvar, lt') := type_and_remove var lt in
    (* easiest way to get pattern variables' type is to redo *)
    (* the process of iteraion over branches and variants labels *)
    (* however we don't have to check for branches unification, has *)
    (* if not correct, the previous typechecking would have failed *)
    (* cons of this methode is that we typecheck twice the branches, *)
    (* might be exponential in the number of nested matches*)
    match tvar with
    | ty_vty (vty_variant vt) =>
      let%err lbr' :=
         (fix typed_branches vt lbr {struct lbr} : M (list (pattern*typed_instr)) :=
            match vt, lbr with
            | nil, nil => Return _ nil
            | nil, _::_ => should_not_happen
            | _, nil => should_not_happen
            | (s1, t1)::tl1, (pattern_any,ins)::tl2 =>
              let%err tins := typed_instr_of_instr ins (rty_record lt') env in
              Return _  ((pattern_any, tins)::nil)
            | (_,t)::tl1, (pattern_variant c l, ins)::tl2 =>
              let%err tins := typed_instr_of_instr ins (rty_record ((l, t)::lt')) env in
              let%err tl' := typed_branches tl1 tl2 in
              Return _ ((pattern_variant c l, tins)::tl')
            | _, _ => should_not_happen
            end)
           vt lbr in
      Return _ (ty_instr_match rt var lbr' rt')
    | ty_or ty1 ty2 =>
      match lbr with
      | (pattern_variant s1 l1, ins1)::(pattern_variant s2 l2, ins2)::nil =>
        let%err ins1' := typed_instr_of_instr ins1 (rty_record ((l1, ty1)::lt')) env in
        let%err ins2' := typed_instr_of_instr ins2 (rty_record ((l2, ty2)::lt')) env in
        Return _ (ty_instr_match rt var ((pattern_variant s1 l1, ins1')::(pattern_variant s2 l2, ins2')::nil) rt')
      | _ => should_not_happen
      end
    | ty_option ty =>
      match lbr with
      | (pattern_variant s1 l1, ins1)::(pattern_variant s2 l2, ins2)::nil =>
        let%err ins1' := typed_instr_of_instr ins1 (rty_record ((l1, ty_unit)::lt')) env in
        let%err ins2' := typed_instr_of_instr ins2 (rty_record ((l2, ty)::lt')) env in
        Return _ (ty_instr_match rt var ((pattern_variant s1 l1, ins1')::(pattern_variant s2 l2, ins2')::nil) rt')
      | _ => should_not_happen
      end
    | ty_bool =>
      match lbr with
      | (pattern_variant s1 l1, ins1)::(pattern_variant s2 l2, ins2)::nil =>
        let%err ins1' := typed_instr_of_instr ins1 (rty_record ((l1, ty_unit)::lt')) env in
        let%err ins2' := typed_instr_of_instr ins2 (rty_record ((l2, ty_unit)::lt')) env in
        Return _ (ty_instr_match rt var ((pattern_variant s1 l1, ins1')::(pattern_variant s2 l2, ins2')::nil) rt')
      | _ => should_not_happen
      end
    | ty_list ty =>
      match lbr with
      | (pattern_nil,ins_nil)::(pattern_cons lhd ltl,ins_cons)::nil =>
        let%err insnil' :=  typed_instr_of_instr ins_nil (rty_record lt') env in
        let%err inscons' :=
           typed_instr_of_instr
             ins_cons (rty_record (LexicoTySort.sort ((lhd, ty)::(ltl, ty_list ty)::lt'))) env in
        Return _ (ty_instr_match rt var ((pattern_nil,insnil')::(pattern_cons lhd ltl,inscons')::nil) rt')
      |  (pattern_nil,ins_nil)::(pattern_any,ins_any)::nil =>
         let%err insnil' := typed_instr_of_instr ins_nil (rty_record lt') env in
         let%err insany' := typed_instr_of_instr ins_any (rty_record lt') env in
         Return _ (ty_instr_match rt var ((pattern_nil,insnil')::(pattern_any,insany')::nil) rt')
      | (pattern_cons lhd ltl,ins_cons)::(pattern_any,ins_any)::nil =>
        let%err inscons' := typed_instr_of_instr ins_cons (rty_record ((lhd, ty)::(ltl, ty_list ty)::lt')) env in
        let%err insany' := typed_instr_of_instr ins_any (rty_record lt') env in
        Return _ (ty_instr_match rt var ((pattern_cons lhd ltl,inscons')::(pattern_any,insany')::nil) rt')
      | _ => should_not_happen
      end
    | _ => should_not_happen
    end
  | instr_loop_left var var' body =>
    let%err rt' := no_tail_fail (type_instr ins rt env) in
    let 'rty_record lt := rt in
    let%err (tvar, lt') := type_and_remove var lt in
    match tvar with
    | ty_or ty1 ty2 | ty_vty (vty_variant (("Left", ty1)::("Right", ty2)::nil))
                      => let%err tbody := typed_instr_of_instr body (rty_record ((var', ty1)::lt')) env in
                        Return _ (ty_instr_loop_left rt var var' tbody rt')
    | _ => Failed _ (Typing "loop should be applied either to bool or an equivalent variant")
    end
  | instr_loop var body =>
    let%err rt' := no_tail_fail (type_instr ins rt env) in
    let 'rty_record lt := rt in
    let%err (_, lt') := type_and_remove var lt in
    let%err tbody := typed_instr_of_instr body (rty_record lt') env in
    Return _ (ty_instr_loop rt var tbody rt')
  | instr_list_for l2 l1 i =>
    let%err rt' := no_tail_fail (type_instr ins rt env) in
    let 'rty_record lt := rt in
    let%err (ty1, l') := type_and_remove l1 lt in
    match ty1 with
    | ty_list ty1' => let%err i' := typed_instr_of_instr i (rty_record ((l2, ty1')::l')) env in
                     Return _ (ty_instr_list_for rt l2 l1 i' rt')
    | _ => Failed _ (Typing "Argument of list_for should be a list")
    end
  | instr_map_for l1 l2 var i =>
    let%err rt' := no_tail_fail (type_instr ins rt env) in
    let 'rty_record lt := rt in
    let%err (ty, l') := type_and_remove var lt in
    match ty with
    | ty_map ty1 ty2 => let%err i' :=
                          typed_instr_of_instr i (rty_record ((l1, ty1)::(l2, ty2)::l')) env in
                       Return _ (ty_instr_map_for rt l1 l2 var i' rt')
    | _ => Failed _ (Typing "Argument of map_for should be a map")
    end
  | instr_failwith a => Return _ (ty_instr_failwith rt a)
  end.

Fixpoint type_prog_aux (defs:prog) (env:g) : M (list (fvar * typed_instr)) :=
  match defs with
  | prog_def (def_tdef _ _) => Return _ nil
  | prog_def (def_fdef fv ty1 ty2 ins) =>
    match ty1, ty2 with
    | ty_rty rty1, ty_rty rty2 =>
      let%err rty2' := no_tail_fail (type_instr ins rty1 env) in
      if (rty_eq rty2 rty2')
      then let%err ins' := typed_instr_of_instr ins rty1 env in
           Return _ ((fv, ins')::nil)
      else Failed _ (Typing ("Return rty of function "++fv++" is incorrect: expected"
                                                      ++(string_of_type (ty_rty rty2))++", found "
                                                      ++(string_of_type (ty_rty rty2'))))
    | _, _ => Failed _ (Typing ("Input and output types function "++fv++" should be record types"))
    end
  | prog_defs (def_tdef _ _) tl => type_prog_aux tl env
  | prog_defs (def_fdef fv ty1 ty2 ins) tl =>
    match ty1, ty2 with
    | ty_rty rty1, ty_rty rty2 =>
      let%err rty2' := no_tail_fail (type_instr ins rty1 env) in
      if (rty_eq rty2 rty2')
      then let%err ins' := typed_instr_of_instr ins rty1 env in
           let%err tl' := type_prog_aux tl (g_fundecl env fv ty1 ty2) in
           Return _ ((fv, ins')::tl')
      else Failed _ (Typing ("Return rty of function "++fv++" is incorrect: expected"
                                                      ++(string_of_type (ty_rty rty2))++", found "
                                                      ++(string_of_type (ty_rty rty2'))))
    | _, _ => Failed _ (Typing ("Input and output types function "++fv++" should be record types"))
    end
  end.

Definition type_program (defs:prog) :=
  let%err defs' := remove_tvar_from_program defs g_empty in
  type_prog_aux (sort_fields_in_program defs') g_empty.

(* Module Tests. *)
(* Open Scope string_scope. *)

(* Import ZArith. *)

(* Compute (type_value (val_constr "A" (val_num (numval_int 12%Z)) (vty_variant (("A", ty_int)::("B", ty_nat)::nil)))). *)

(* Definition startType := (rty_record ( *)
(*                              ("b", ty_bool) *)
(*                                ::("m", ty_map ty_nat ty_nat) *)
(*                                ::("or", ty_or ty_int ty_nat) *)
(*                                ::("v", ty_vty (vty_variant (("A", ty_int)::("B", ty_nat)::nil))) *)
(*                                ::("x", ty_int) *)
(*                                ::("x2", ty_nat) *)
(*                                ::("z", ty_rty (rty_record (("a", ty_int)::("b", ty_mutez)::("c", ty_int)::("d", ty_string)::nil)))::nil)). *)

(* Compute (type_instr (instr_assign (lhs_var "y") (rhs_arg (arg_var "x"))) startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_var "p") (rhs_projection "z" "b")) startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_record (("a","c")::nil)) (rhs_arg (arg_var "z"))) startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_var "r") (rhs_arg (arg_record (("a1", "x")::("a2","z")::nil)))) startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_var "r") (rhs_update "z" (("c", "x")::nil))) startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_var "s") (rhs_add "x" "x2")) startType g_empty). *)
(* Compute (type_instr (instr_drop "x2") startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_var "v2") (rhs_app (fun_constr "A" (vty_variant (("A", ty_int)::("B", ty_nat)::nil))) (arg_var "x"))) startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_var "res") (rhs_match "v" (branchesrhs_branches (("A", "i", (rhs_arg (arg_var "i"))) *)
(*                                                                                           ::("B", "n", (rhs_arg (arg_value (val_num (numval_int 2%Z)))))::nil)))) startType g_empty). *)
(* Compute (type_instr (instr_match "v" (branchesins_branches (("A", "i", (instr_seq (instr_drop "x") *)
(*                                                                                   (instr_assign (lhs_var "r") (rhs_arg (arg_var "i"))))) *)
(*                                                               ::("B", "n", (instr_seq (instr_drop "n") *)
(*                                                                                       (instr_assign (lhs_var "r") (rhs_arg (arg_var "x")))))::nil))) startType g_empty). *)
(* Compute (type_instr (instr_assign (lhs_var "m2") (rhs_map_map "k" "e" "m" (rhs_add "k" "e"))) startType g_empty). *)
(* Compute (typed_instr_of_instr *)
(*            (instr_loop_left "or" "o" (instr_assign (lhs_var "or") *)
(*                                                    (rhs_app (fun_constr "Left" (vty_variant (("Left", ty_int)::("Right", ty_nat)::nil))) *)
(*                                                             (arg_var "o")))) *)
(*         startType g_empty). *)
(* Compute (typed_instr_of_instr *)
(*            (instr_loop "b" (instr_assign (lhs_var "b") *)
(*                                          (rhs_app (fun_constr "True" (vty_variant (("False", ty_unit)::("True", ty_unit)::nil))) *)
(*                                                   (arg_value (val_rval (rval_record nil)))))) *)
(*         startType g_empty). *)
(* Compute (type_instr (instr_seq *)
(*                        (instr_assign (lhs_var "b2") (rhs_app (fun_constr "True" (vty_variant (("False", ty_unit)::("True", ty_unit)::nil))) (arg_value (val_rval (rval_record nil))))) *)
(*                        (instr_assign (lhs_var "b3") (rhs_or "b" "b2"))) startType g_empty). *)

(* Compute (type_program (prog_defs *)
(*                          (def_tdef "int" ty_int) *)
(*                          (prog_defs (def_tdef "int+nat" (ty_vty (vty_variant (("A", (ty_var "int"))::("B", ty_nat)::nil)))) *)
(*                          (prog_defs (def_fdef "f0" (ty_rty (rty_record (("i", ty_int)::nil))) *)
(*                                      (ty_rty (rty_record (("v", (ty_var "int+nat"))::nil))) *)
(*                                      (instr_assign (lhs_var "v") (rhs_app (fun_constr "A" (vty_variant (("A", ty_int)::("B", ty_nat)::nil))) (arg_var "i")))) *)
(*                          (prog_def (def_fdef "f1" (ty_rty (rty_record (("ri", (ty_rty (rty_record (("i", ty_int)::nil))))::nil))) *)
(*                                      (ty_rty (rty_record (("rv", (ty_rty (rty_record (("v", (ty_var "int+nat"))::nil))))::nil))) *)
(*                            (instr_assign (lhs_var "rv") (rhs_app (fun_var "f0") (arg_var "ri")))) *)
(*                          ))))). *)

(* End Tests. *)
