(* Error Monad *)

Require Import String.

Inductive exception : Set :=
| Not_found (lab:string)
| Typing (msg:string)
| Overflow
| Should_not_happen
| Not_yet_implemented (funct:string).

Inductive M (A : Type) : Type :=
| Failed : exception -> M A
| Return : A -> M A.

Definition bind {A B} (f : A -> M B) (m : M A) :=
  match m with
  | Failed _ e => Failed _ e
  | Return _ SB => f SB
  end.

Notation "m >>= f" := (bind f m) (at level 60, f at next level ).

Notation "'let%err' binder := exp1 'in' exp2":=
  (exp1 >>= fun binder => exp2) (at level 200,binder pattern, exp1 at level 200, exp2 at level 200) : monadic_let_scope.

Lemma bind_eq_return {A B} f m b :
  bind f m = Return B b ->
  exists a : A, m = Return A a /\ f a = Return B b.
Proof.
  destruct m.
  - discriminate.
  - simpl.
    exists a.
    auto.
Qed.

Open Scope monadic_let_scope.
Fixpoint map {A} {B} (f : A -> M B) (l:list A) : M (list B):=
  (fix aux l res :=
     match l with
     | nil =>  Return _ (List.rev res)
     | cons hd tl =>
       let%err b := f hd in
       aux tl (cons b res)
     end)
    l nil.
Close Scope monadic_let_scope.

Open Scope string_scope.

Definition string_of_exception (e:exception) : string :=
  match e with
  | Not_found lab => "Var '"++lab++"' not found in environment"
  | Typing msg => "Typing failure : "++msg
  | Overflow => "Integer overflow"
  | Should_not_happen => "This exception should never happen, this is a bug"
  | Not_yet_implemented funct => "The feature '"++funct++"' has not yet been implemented"

  end.
