Require Import String.

(** Arithmetics *)
Inductive arith_op : Set :=
  | Add | Sub | Mul | Div | Rem | And | Or | Xor | Lsl | Lsr.

Definition not_num (nv:num_val) : num_val := nv. (* TODO *)
Definition bin_arith (op:arith_op) (nv1:num_val) (nv2:num_val) : num_val := nv1. (* TODO *)
Definition abs (nv:num_val) : num_val := nv. (* TODO *)

(** Comparisons *)
Inductive cmp_op : Set :=
  | Eq | Neq | Inf | Sup | InfEq | SupEq.

Axiom compare_num : cmp_op -> num_val -> num_val -> Prop.
Axiom compare_str : cmp_op -> string_val -> string_val -> Prop.
Axiom compare_bytes : cmp_op -> bytes_val -> bytes_val -> Prop.

(* String operations *)
Definition slice (nv1:num_val) (nv2:num_val) (s:string) := s. (* TODO *)

(* Bytes operations *)
Definition pack (_:value) : string_val := EmptyString. (* TODO *)
Definition unpack (s:string_val) : value := (val_rval (rval_record nil)). (* TODO *)

(* Domain specific operations *)
Definition get_contract (a:add_val) : add_val := a. (* TODO *)
Definition get_address (a:add_val) : add_val := a. (* TODO *)
Definition get_implicit_account (a:add_val) : add_val := a. (* TODO *)