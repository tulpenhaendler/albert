open Lexer
open Lexing
open Parser

(* module ContractContext : Syntax_type.ContractContext = struct
 *   let get_contract_type _ = Error.Failed (Error.Typing (Aux.explode "get_contract_type unvailable")))
 * end *)

(* module Compiler = Michocomp.Michocomp (struct let selfType = Albert.Coq_ty_unit end) (ContractContext) *)

let print_position outx lexbuf =
  let pos = lexbuf.lex_curr_p in
  Printf.fprintf outx "%s:%d:%d" pos.pos_fname
    pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse_with_error lexbuf =
  try Parser.prog_start Lexer.token lexbuf with
  | Parser.Error ->
    Printf.fprintf stderr "Syntax error in program at %a\n" print_position lexbuf;
    exit (-1)

let pretty_print_body ins =
  Aux.implode (Micheline_pp.micheline_pp
                 (Michelson2micheline.michelson2micheline_instruction ins) Datatypes.O false true)

let print_function ppf (((fv, t1), ins), t2) =
  Format.fprintf ppf "@[<v 2>@[<h>%s: %s -> %s =@]@[%s@]@."
    (Aux.implode fv)
    (Aux.implode (Micheline_pp.micheline_pp (Michelson2micheline.michelson2micheline_type t1) Datatypes.O false false))
    (Aux.implode (Micheline_pp.micheline_pp (Michelson2micheline.michelson2micheline_type t2) Datatypes.O false false))
    (pretty_print_body ins)

let rec print_functions ppf lf =
  match lf with
  | [] -> ()
  | hd::tl -> print_function ppf hd; print_functions ppf tl

(** Print an actual michelson program *)
let print_program channel ty_param ty_storage code =
  Format.fprintf channel "@[<v 0>@[parameter %s;@]@,storage @[%s;@]@,code @[%s@]"
    (Aux.implode (Micheline_pp.micheline_pp (Michelson2micheline.michelson2micheline_type ty_param) Datatypes.O false false))
    (Aux.implode (Micheline_pp.micheline_pp (Michelson2micheline.michelson2micheline_type ty_storage) Datatypes.O false false))
    (Aux.implode (Micheline_pp.micheline_pp (Michelson2micheline.michelson2micheline_instruction code)
                    (Datatypes.length (Aux.explode "code ")) false true))

let check_prog_type_and_print channel (((_, t1), code), t2) =
  match t1, t2 with
  | Syntax_type.Coq_pair (t_par, t_sto), Syntax_type.Coq_pair (Syntax_type.Coq_list (Syntax_type.Coq_operation), t_newsto) ->
    if t_sto = t_newsto then print_program channel t_par t_sto code
    else Format.eprintf "The type of storage does not match the code type\n"
  | _, _ -> Printf.fprintf stderr "The entry point function does not have a correct type for a michelson program\n"




let speclist = [
]

type output_format = Albert | Michelson

let main () =
  let output_filename = ref None in
  let input_filename = ref None in
  let entrypoint = ref None in
  let oformat = ref Michelson in
  let parse_list =
    [("-o",
      Arg.String (fun s -> output_filename := Some s),
      ": output file (default is input basename with tz extension)") ;
     ("-entrypoint",
      Arg.String (fun s -> entrypoint := Some s), ": entry point for the program");
     ("-output-format",
      Arg.String (function "Albert" | "albert" | "alb" -> oformat := Albert | _ -> ()),
      ": output format (either Albert or Michelson, default is Michelson)")
    ] in
  let usage = "usage: " ^ Sys.argv.(0) ^ " [options] <input_file>" in
  Arg.parse parse_list (fun input -> input_filename := Some input) usage;
  match !input_filename with
    None -> Format.printf "No input given, gimme fooood !"; exit 1
  | Some input_filename -> 
     let ic = open_in input_filename in
     let oc = Format.formatter_of_out_channel @@
       open_out (match !output_filename with
                 | None -> (Filename.remove_extension input_filename)^".tz"
                 | Some name -> name)  in
     let prog = parse_with_error (Lexing.from_channel ic) in
     match !oformat with
     | Michelson -> begin
       match (Michocomp.type_and_compile_prog prog) with
       | Failed e ->
          print_endline
            (Aux.implode (Error.string_of_exception e));
          exit 1
       | Return lf ->
          (match !entrypoint with
           | None -> print_functions oc lf
           | Some e ->
               (try check_prog_type_and_print oc
                   (Stdlib.List.find (fun (((fv, _), _), _) -> (Aux.implode fv) = e) lf)
                with _ ->
                 Printf.fprintf stderr "The entry point %s was not found in the program\n" e;exit 2))
       end
     | Albert -> Format.fprintf oc "%s" (Aux.implode (Pretty_printers.string_of_prog prog))



let _ = main ()
