(* Auxiliary functions *)

(** Explode a string into a list of characters *)
val explode : string -> char list

(** Opposite of explode, recreate a string from a list of characters *)
val implode : char list -> string

(** Parse a string to an unsigned integer *)
val uint_of_string : string -> Decimal.uint

(** Parse a string to an integer *)
val int_of_string : string -> Decimal.int

(** Trim the borders of the string *)
val trim : string -> string
