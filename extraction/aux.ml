(* Auxiliary functions *)

let explode s =
  let rec exp i l =
    if i < 0 then l else exp (i - 1) ((Stdlib.String.get s i) :: l) in
  exp (Stdlib.String.length s - 1) []

let implode l =
  Stdlib.String.init (Stdlib.List.length l) (Stdlib.List.nth l)

let rec uint_of_string_aux cs =
  match cs with
  | [] -> Decimal.Nil
  | '0'::tl -> Decimal.D0 (uint_of_string_aux tl)
  | '1'::tl -> Decimal.D1 (uint_of_string_aux tl)
  | '2'::tl -> Decimal.D2 (uint_of_string_aux tl)
  | '3'::tl -> Decimal.D3 (uint_of_string_aux tl)
  | '4'::tl -> Decimal.D4 (uint_of_string_aux tl)
  | '5'::tl -> Decimal.D5 (uint_of_string_aux tl)
  | '6'::tl -> Decimal.D6 (uint_of_string_aux tl)
  | '7'::tl -> Decimal.D7 (uint_of_string_aux tl)
  | '8'::tl -> Decimal.D8 (uint_of_string_aux tl)
  | '9'::tl -> Decimal.D9 (uint_of_string_aux tl)
  | _ -> invalid_arg "uint_of_string_aux"

let uint_of_string s =
  let chars = explode s in (uint_of_string_aux chars)

let int_of_string s =
  let chars = explode s in
  match chars with
  | '+'::tl -> Decimal.Pos (uint_of_string_aux tl)
  | '-'::tl -> Decimal.Neg (uint_of_string_aux tl)
  | _ -> invalid_arg "int_of_string"

let trim s =
  Stdlib.String.sub s 1 (Stdlib.String.length s - 2)
